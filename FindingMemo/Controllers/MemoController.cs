﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using FindingMemo.Models;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace FindingMemo.Controllers {
    /// <summary>
    /// Format and manage the data receive from the database for the view
    /// </summary>
    public class MemoController
    {
        #region Models
        private AuthManager _aM;
        private LessonManager _lM;
        private ClassManager _cM;
        private PeopleManager _peM;
        private PresenceManager _prM;
        private PlanningManager _plM;
        #endregion

        private People _connectedUser;
        private PresenceMode _currentPresenceMode;
        private Dictionary<PresenceMode, Color> _presenceModeProperties;

        private People ConnectedUser { get => _connectedUser; set => _connectedUser = value; }
        public PresenceMode CurrentPresenceMode { get => _currentPresenceMode; set => _currentPresenceMode = value; }
        public Dictionary<PresenceMode, Color> PresenceModeProperties { get => _presenceModeProperties; set => _presenceModeProperties = value; }

        private AuthManager AM { get => _aM; set => _aM = value; }
        private LessonManager LM { get => _lM; set => _lM = value; }
        private ClassManager CM { get => _cM; set => _cM = value; }
        private PeopleManager PeM { get => _peM; set => _peM = value; }
        private PresenceManager PrM { get => _prM; set => _prM = value; }
        private PlanningManager PlM { get => _plM; set => _plM = value; }

        public MemoController()
        {
            AM = new AuthManager();
            LM = new LessonManager();
            CM = new ClassManager();
            PeM = new PeopleManager();
            PrM = new PresenceManager();
            PlM = new PlanningManager();

            CurrentPresenceMode = PresenceMode.Présent;
            PresenceModeProperties = new Dictionary<PresenceMode, Color>()
            {
                {PresenceMode.Présent,Color.LightGreen},
                {PresenceMode.Absent,Color.IndianRed},
                {PresenceMode.Libéré,Color.SkyBlue}
            };

        }


        #region PresenceMode
        public PresenceMode GetUserStatusId(int idUser, int idLesson, DateTime datePresence) {
            return PrM.GetUserStatusId(idUser, idLesson, datePresence);
        }
        public string GetEnumString(PresenceMode p) {
            return Enum.GetName(typeof(PresenceMode), p);
        }
        public enum PresenceMode {
            Présent = 1,
            Absent = 2,
            Libéré = 3
        }
        #endregion PresenceMode

        #region AuthManager
        public string GetNameConnectedUser() {
            return string.Format("{0} {1}", ConnectedUser.firstName, ConnectedUser.name);
        }
        public string GetEmailConnectedUser() {
            return ConnectedUser.email;
        }
        public string GetBirthDateConnectedUser() {
            return ConnectedUser.birthDate?.ToString("dd.MM.yy");
        }
        public string GetLoginConnectedUser() {
            return ConnectedUser.login;
        }
        public int GetIdConnectedUser() {
            return ConnectedUser.idPeople;
        }
        public bool IsConnectedUserAdmin() {
            return ConnectedUser.isAdmin == 1;
        }
        public bool VerifyLogin(string login, string password) {
            ConnectedUser = AM.VerifyLogin(login, password);

            // Return true if the login has succedeed
            return (ConnectedUser != null);
        }
        #endregion AuthManager

        #region ClassManager
        public List<Classes> GetAllClasses() {
            return CM.GetAllClasses();
        }

        public Classes GetClassesById(int idClass) {
            return CM.GetClassesById(idClass);
        }
        public Dictionary<int, string> GetClassesByTeacher(int idTeacher) {
            return CM.GetClassesByTeacher(idTeacher);
        }
        public bool AddClass(string className, int capacity) {
            return CM.AddClass(className, capacity);
        }
        public bool EditClass(int idClass, string className, int capacity) {
            return CM.EditClass(idClass, className, capacity);
        }
        #endregion ClassManager

        #region PeopleManager
        public List<People> GetAllPeopleExceptAdmin() {
            return PeM.GetAllPeopleExceptAdmin();
        }
        public List<People> GetAllTeachers() {
            return PeM.GetAllTeachers();
        }
        public People GetPeopleById(int idPeople) {
            return PeM.GetPeopleById(idPeople);
        }
        public Dictionary<int, string> GetStudentsByClass(int idClass) {
            return PeM.GetStudentsByClass(idClass);
        }
        public void AddStudent(string name, string firstName, DateTime birthDate, string email, string phone, int idClass) {
            PeM.AddStudent(name, firstName, birthDate, email, phone, idClass);
        }
        public void AddTeacher(string name, string firstName, DateTime birthDate, string email, string phone, string login, string password) {
            PeM.AddTeacher(name, firstName, birthDate, email, phone, login, password);
        }
        public bool EditPassword(string oldPassword, string newPassword) {
            return PeM.EditPassword(ConnectedUser.idPeople, oldPassword, newPassword);
        }
        public void EditStudent(int idPeople, string name, string firstName, DateTime birthDate, string email, string phone, int idClass) {
            PeM.EditStudent(idPeople, name, firstName, birthDate, email, phone, idClass);
        }

        public void EditTeacher(int idPeople, string name, string firstName, DateTime birthDate, string email, string phone, string login, string password) {
            PeM.EditTeacher(idPeople, name, firstName, birthDate, email, phone, login, password);
        }
        public bool DeletePeopleById(int idPeople) {
            return PeM.DeletePeopleById(idPeople);
        }
        #endregion

        #region LessonManager
        public List<Lessons> GetAllLessons() {
            return LM.GetAllLessons();
        }
        public Lessons GetLessonsById(int idLesson) {
            return LM.GetLessonById(idLesson);
        }
        public Dictionary<int, string> GetLessonsByTeacher(int dayOfTheWeek, int idClass) {
            return LM.GetLessonsByTeacher(dayOfTheWeek, ConnectedUser.idPeople, idClass);
        }
        public void AddLesson(string label, string description) {
            LM.AddLesson(label, description);
        }
        public void EditLesson(int idLesson, string label, string description) {
            LM.EditLesson(idLesson, label, description);
        }
        public bool DeleteLesson(int idLesson) {
            return LM.DeleteLesson(idLesson);
        }
        #endregion LessonManager

        #region PresenceManager
        public bool SavePresence(int idPeople, int idLesson, int idStatus, DateTime value) {
            return PrM.SavePresence(idPeople, idLesson, idStatus, value);
        }
        public bool UpdatePresences(Dictionary<int, int> presencesToEdit, DateTime date, int idLesson) {
            return PrM.UpdatePresences(presencesToEdit, date, idLesson);
        }
        #endregion PresenceManager

        #region PlanningManager
        public List<Plannings> GetAllPlannings() {
            return PlM.GetAllPlannings();
        }
        public bool AssignTeacherAndClassesToLesson(int idLesson, int idClass, int idTeacher, int dayOfLesson) {
            return PlM.AssignTeacherAndClassesToLesson(idLesson, idClass, idTeacher, dayOfLesson);
        }


        public bool EditAssignement(int idLesson, int dayOfLesson, int idClass, int newDayOfLesson, int newIdClass, int newIdTeacher)
        {
            return PlM.EditAssignement(idLesson,dayOfLesson,idClass,newDayOfLesson,newIdClass,newIdTeacher);
        }

        public bool DeleteAssignement(int idLesson, int idClass, int dayOfLesson) {
            return PlM.DeleteAssignement(idLesson, idClass, dayOfLesson);
        }
        #endregion PlanningManager



    }
}
