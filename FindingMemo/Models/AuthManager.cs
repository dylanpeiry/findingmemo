﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 0.1
 * Date : 01.11.2018
 */
using System;
using System.Linq;

namespace FindingMemo.Models
{
    /// <summary>
    /// Manage the requests to authentify the user
    /// </summary>
    public class AuthManager
    {
        private MemoDBDataContext _memoDB;
        private Tools _t;

        private MemoDBDataContext MemoDB { get => _memoDB; set => _memoDB = value; }
        private Tools T { get => _t; set => _t = value; }

        public AuthManager()
        {
            MemoDB = new MemoDBDataContext();
            T = new Tools(null);
        }

        public People VerifyLogin(string login, string password)
        {
            try
            {
                var result = (from People in MemoDB.People
                              where People.login == login && People.password == T.GetHashSha256(password)
                              select People).Single();
                return (People)result;
            }
            catch (InvalidOperationException e)
            {
                return null;
            }
        }
    }
}
