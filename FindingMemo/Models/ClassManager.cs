﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace FindingMemo.Models {
    public class ClassManager
    {
        private MemoDBDataContext _memoDB;

        private MemoDBDataContext MemoDB { get => _memoDB; set => _memoDB = value; }

        public ClassManager()
        {
            MemoDB = new MemoDBDataContext();
        }

        public Dictionary<int, string> GetClassesByTeacher(int idTeacher)
        {
            var resultClasses = from Plannings in MemoDB.Plannings
                         join Classes in MemoDB.Classes
                         on Plannings.idClass equals Classes.idClass
                         where Plannings.idTeacher == idTeacher
                         select new { Classes.idClass,Classes.labelClass};


            Dictionary<int, string> classes = new Dictionary<int, string>();
            foreach (var c in resultClasses)
            {
                if (!classes.ContainsKey(c.idClass))
                    classes.Add(c.idClass, c.labelClass);
            }

            return classes;
        }

        public List<Classes> GetAllClasses() {
            return (from Classes in MemoDB.Classes select Classes).ToList();
        }

        public bool AddClass(string className, int capacity)
        {
            Classes c = new Classes
            {
                labelClass = className,
                capacity = capacity
            };

            MemoDB.Classes.InsertOnSubmit(c);

            try
            {
                MemoDB.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Classes GetClassesById(int idClass)
        {
            return (from Classes in MemoDB.Classes
                    where Classes.idClass == idClass
                    select Classes).Single();
        }

        public bool EditClass(int idClass, string className, int capacity)
        {
            var c = (from Classes in MemoDB.Classes
                     where Classes.idClass == idClass
                     select Classes).Single();

            c.labelClass = className;
            c.capacity = capacity;

            try
            {

                MemoDB.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}