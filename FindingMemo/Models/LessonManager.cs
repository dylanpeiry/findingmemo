﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindingMemo.Models
{
    public class LessonManager
    {
        private MemoDBDataContext _memoDB;

        private MemoDBDataContext MemoDB { get => _memoDB; set => _memoDB = value; }

        public LessonManager()
        {
            MemoDB = new MemoDBDataContext();
        }
        public List<Lessons> GetAllLessons() {
            return (from Lesson in MemoDB.Lessons
                    select Lesson).ToList();
        }

        public Dictionary<int, string> GetLessonsByTeacher(int dayOfTheWeek, int idTeacher, int idClass)
        {
            var result = from Plannings in MemoDB.Plannings
                         join Lessons in MemoDB.Lessons
                         on Plannings.idLesson equals Lessons.idLesson
                         where Plannings.idTeacher == idTeacher && Plannings.dayOfLesson == dayOfTheWeek && Plannings.idClass == idClass
                         select new { Lessons.idLesson, Lessons.labelLesson };

            Dictionary<int, string> lessons = new Dictionary<int, string>();
            foreach (var l in result)
            {
                lessons.Add(l.idLesson, l.labelLesson);
            }

            return lessons;
        }

        public Lessons GetLessonById(int id) {
            return (from Lesson in MemoDB.Lessons
                    where Lesson.idLesson == id
                    select Lesson).Single();
        }

        public void AddLesson(string label, string description) {
            Lessons lesson = new Lessons() {
                labelLesson = label,
                description = description
            };

            MemoDB.Lessons.InsertOnSubmit(lesson);
            MemoDB.SubmitChanges();
        }
        
        public void EditLesson(int idLesson, string label, string description) {
            Lessons lesson = GetLessonById(idLesson);

            lesson.labelLesson = label;
            lesson.description = description;
            MemoDB.SubmitChanges();
        }

        public bool DeleteLesson(int idLesson) {
            // First we delete the plannings of the lesson
            var plannings = from Plannings in MemoDB.Plannings
                            where Plannings.idLesson == idLesson
                            select Plannings;
            MemoDB.Plannings.DeleteAllOnSubmit(plannings);

            // Next we delete the presences of the lessons
            // TODO: don't repeat yourself
            var presences = from Presences in MemoDB.Presences
                            where Presences.idPeople == idLesson
                            select Presences;
            MemoDB.Presences.DeleteAllOnSubmit(presences);

            // Then we can delete the lesson
            Lessons lesson = GetLessonById(idLesson);
            MemoDB.Lessons.DeleteOnSubmit(lesson);

            try {
                MemoDB.SubmitChanges();
                return true;
            } catch (Exception) {
                return false;
            }
        }
    }
}
