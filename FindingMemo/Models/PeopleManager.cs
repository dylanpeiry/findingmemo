﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace FindingMemo.Models {
    public class PeopleManager
    {
        private MemoDBDataContext _memoDb;
        private Tools _t;

        private MemoDBDataContext MemoDB { get => _memoDb; set => _memoDb = value; }
        private Tools T { get => _t; set => _t = value; }

        /// <summary>
        /// Value who indicate that the people is a teacher
        /// </summary>
        private const int IS_TEACHER = 1;


        public PeopleManager()
        {
            MemoDB = new MemoDBDataContext();
            T = new Tools(null);
        }

        public Dictionary<int, string> GetStudentsByClass(int idClass)
        {
            var result = from People in MemoDB.People
                         where People.idClass == idClass
                         select new { People.idPeople, LastFirstName = string.Format("{0} {1}", People.name, People.firstName) };

            Dictionary<int, string> students = new Dictionary<int, string>();
            foreach (var s in result)
            {
                students.Add(s.idPeople, s.LastFirstName);
            }
            return students;
        }

        public List<People> GetAllPeopleExceptAdmin() {
            return (from People in MemoDB.People
                    where People.isAdmin == 0
                    select People).ToList();
        }

        public List<People> GetAllTeachers() {
            return (from People in MemoDB.People
                    where People.isTeacher == 1
                    where People.isAdmin == 0
                    select People).ToList();
        }

        public People GetPeopleById(int idPeople)
        {
            return (from People in MemoDB.People
                          where People.idPeople == idPeople
                          select People).Single();
        }

        /// <summary>
        /// Add a new student to the people table
        /// </summary>
        /// <param name="name">Name of the student</param>
        /// <param name="firstName">First name of the student</param>
        /// <param name="birthDate">Birthdate of the student. Can be null</param>
        /// <param name="email">Email of the student</param>
        /// <param name="phone">Phone of the student. Can be null</param>
        /// <param name="idClass">The id of the class of the student</param>
        public void AddStudent(string name, string firstName, DateTime? birthDate, string email, string phone, int idClass)
        {
            People people = new People
            {
                name = name,
                firstName = firstName,
                birthDate = birthDate,
                email = email,
                phone = phone,
                idClass = idClass
            };
            //TODO: verify that the class is not full

            MemoDB.People.InsertOnSubmit(people);
            MemoDB.SubmitChanges();
        }

        public void AddTeacher(string name, string firstName, DateTime? birthDate, string email, string phone, string login, string password)
        {
            // hash the password
            password = T.GetHashSha256(password);

            People people = new People
            {
                name = name,
                firstName = firstName,
                birthDate = birthDate,
                email = email,
                phone = phone,
                isTeacher = IS_TEACHER,
                login = login,
                password = password,
            };

            MemoDB.People.InsertOnSubmit(people);
            MemoDB.SubmitChanges();
        }

        public bool EditPassword(int idPeople, string oldPassword, string newPassword)
        {
            try
            {
                People p = (from People in MemoDB.People
                         where People.password == T.GetHashSha256(oldPassword) && People.idPeople == idPeople
                         select People).Single();

                p.password = T.GetHashSha256(newPassword);
                MemoDB.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                // Discard changes
                MemoDB = new MemoDBDataContext();
                return false;
            }
        }

        public void EditStudent(int idPeople, string name, string firstName, DateTime? birthDate, string email, string phone, int idClass)
        {
            People p = GetPeopleById(idPeople);
            p.name = name;
            p.firstName = firstName;
            p.birthDate = birthDate;
            p.email = email;
            p.phone = phone;
            p.idClass = idClass;

            MemoDB.SubmitChanges();
        }

        public void EditTeacher(int idPeople, string name, string firstName, DateTime? birthDate, string email, string phone, string login, string password)
        {
            People p = GetPeopleById(idPeople);
            p.name = name;
            p.firstName = firstName;
            p.birthDate = birthDate;
            p.email = email;
            p.phone = phone;
            p.login = login;

            // Verify that the password receveided in parameter is not the same password in the database
            if (p.password != password)
            {
                // hash the (new) password
                password = T.GetHashSha256(password);
                p.password = password;
            }

            MemoDB.SubmitChanges();
        }

        public bool DeletePeopleById(int idPeople)
        {
            // First we need to delete the presence of the people
            // TODO: don't repeat yourself
            var presences = from Presences in MemoDB.Presences
                            where Presences.idPeople == idPeople
                            select Presences;
            MemoDB.Presences.DeleteAllOnSubmit(presences);

            // Then we can delete the people
            People people = GetPeopleById(idPeople);
            MemoDB.People.DeleteOnSubmit(people);

            try
            {
                MemoDB.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                // Discard changes
                MemoDB = new MemoDBDataContext();
                return false;
            }
        }
    }
}