﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using System;
using System.Collections.Generic;
using System.Linq;

namespace FindingMemo.Models {
    public class PlanningManager {

        private MemoDBDataContext _memoDb;

        private MemoDBDataContext MemoDB { get => _memoDb; set => _memoDb = value; }

        public PlanningManager() {
            MemoDB = new MemoDBDataContext();
        }

        public List<Plannings> GetAllPlannings() {
            return (from Planning in MemoDB.Plannings
                    select Planning).ToList();
        }

        /// <summary>
        /// Assign a teacher and classes to a lesson of a specifical day of the week
        /// </summary>
        public bool AssignTeacherAndClassesToLesson(int idLesson, int idClass, int idTeacher, int dayOfLesson) {
            Plannings planning = new Plannings {
                idClass = idClass,
                idLesson = idLesson,
                dayOfLesson = (byte)dayOfLesson,
                idTeacher = idTeacher
            };

            try {
                MemoDB.Plannings.InsertOnSubmit(planning);
                MemoDB.SubmitChanges();
                return true;
            } catch (Exception) {
                // Discard changes
                MemoDB = new MemoDBDataContext();
                return false;
            }
        }

        public Plannings GetPlanningsByIds(int idLesson, int idClass, int dayOfLesson) {
            return (from Planning in MemoDB.Plannings
                    where Planning.idLesson == idLesson
                    where Planning.idClass == idClass
                    where Planning.dayOfLesson == dayOfLesson
                    select Planning).Single();
        }

        /// <summary>
        /// Planifier un cours avec un enseignant, un jour et une classe
        /// </summary>
        /// <param name="idLesson"></param>
        /// <param name="dayOfLesson"></param>
        /// <param name="idClass"></param>
        /// <param name="idTeacher"></param>
        /// <returns></returns>
        public bool EditAssignement(int idLesson, int dayOfLesson, int idClass, int newDayOfLesson,int newIdClass, int newIdTeacher)
        {
            var planning = (from Plannings in MemoDB.Plannings
                            where Plannings.idLesson == idLesson && Plannings.idClass == idClass && Plannings.dayOfLesson == dayOfLesson
                          select Plannings).Single();

            planning.dayOfLesson = (byte)newDayOfLesson;
            planning.idClass = newIdClass;
            planning.idTeacher = newIdTeacher;
            try
            {
                MemoDB.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteAssignement(int idLesson, int idClass, int dayOfLesson) {
            Plannings planning = GetPlanningsByIds(idLesson, idClass, dayOfLesson);

            MemoDB.Plannings.DeleteOnSubmit(planning);
            try {
                MemoDB.SubmitChanges();
                return true;
            } catch (Exception) {
                // Discard changes
                MemoDB = new MemoDBDataContext();
                return false;
            }
        }
    }
}
