﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using FindingMemo.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FindingMemo.Models
{
    public class PresenceManager
    {
        private MemoDBDataContext _memoDb;

        private MemoDBDataContext MemoDB { get => _memoDb; set => _memoDb = value; }

        public PresenceManager()
        {
            MemoDB = new MemoDBDataContext();
        }

        public MemoController.PresenceMode GetUserStatusId(int idUser, int idLesson, DateTime datePresence)
        {
            try
            {
                return (MemoController.PresenceMode)(from Presences in MemoDB.Presences
                                                     where Presences.idPeople == idUser && Presences.datePresence == datePresence && Presences.idLesson == idLesson
                                                     select Presences.idStatus).Single();
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public bool SavePresence(int idPeople, int idLesson, int idStatus, DateTime date)
        {
            Presences p = new Presences
            {
                idPeople = idPeople,
                idLesson = idLesson,
                idStatus = idStatus,
                datePresence = date
            };
            MemoDB.Presences.InsertOnSubmit(p);

            try
            {
                MemoDB.SubmitChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        /// <summary>
        /// Met à jour les présences des élèves qui ont été modifiées seulement
        /// </summary>
        /// <param name="presencesToEdit"></param>
        /// <param name="date"></param>
        /// <param name="idLesson"></param>
        public bool UpdatePresences(Dictionary<int, int> presencesToEdit, DateTime date, int idLesson)
        {
            foreach (KeyValuePair<int, int> item in presencesToEdit)
            {
                var studentPresence = (from Presences in MemoDB.Presences
                                       where Presences.idPeople == item.Key && Presences.idLesson == idLesson && Presences.datePresence == date
                                       select Presences).Single();

                studentPresence.idStatus = item.Value;

                try
                {
                    MemoDB.SubmitChanges();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return true;
        }
    }
}