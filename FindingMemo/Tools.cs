﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace FindingMemo {
    public class Tools
    {
        #region Window's drag settings
        private const int WM_NCLBUTTONDOWN = 0xA1;
        private const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        private static extern bool ReleaseCapture();
        private Form _frm;

        public Form Frm { get => _frm; set => _frm = value; }
        #endregion

        public Tools(Form f)
        {
            Frm = f;
        }

        /// <summary>
        /// Pouvoir déplacer la fenêtre sans bords
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void TitleBar_MouseDown(object sender,MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Frm.Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        /// <summary>
        /// Fenêtre modale de confirmation pour quitter l'application
        /// </summary>
        /// <param name="e"></param>
        public void PromptExit(FormClosingEventArgs e)
        {
            DialogResult dr = MessageBox.Show("Etes-vous sur de vouloir quitter l'application ?", "Fermeture de l'application", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            e.Cancel = dr != DialogResult.Yes;
        }

        /// <summary>
        /// Encrypts the received text into a SHA256 string 
        /// </summary>
        /// <param name="text">The text to encrypt</param>
        /// <returns>The encrypted text in SHA256</returns>
        public string GetHashSha256(string text)
        {
            string hashString = "";

            // Encode the text in UTF8 and get the bytes of each character in a table
            byte[] bytes = Encoding.UTF8.GetBytes(text);

            // Hash our array of byte and stock it in a new array
            SHA256Managed sha256Managed = new SHA256Managed();
            byte[] hash = sha256Managed.ComputeHash(bytes);

            // Add ech byte of our array into our string
            foreach (byte b in hash)
            {
                hashString += String.Format("{0:x2}", b); // b is formatted in hexadecimal
            }

            return hashString;
        }
    }
}