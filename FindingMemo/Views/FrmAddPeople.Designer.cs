﻿namespace FindingMemo.Views {
    partial class FrmAddPeople {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.LblIsTeacher = new System.Windows.Forms.Label();
            this.CbxIsTeacher = new System.Windows.Forms.CheckBox();
            this.TbxPassword = new System.Windows.Forms.TextBox();
            this.LblPassword = new System.Windows.Forms.Label();
            this.TbxLogin = new System.Windows.Forms.TextBox();
            this.LblLogin = new System.Windows.Forms.Label();
            this.TbxPhone = new System.Windows.Forms.TextBox();
            this.LblPhone = new System.Windows.Forms.Label();
            this.TbxEmail = new System.Windows.Forms.TextBox();
            this.LblEmail = new System.Windows.Forms.Label();
            this.DtpBirthDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.TbxFirstName = new System.Windows.Forms.TextBox();
            this.LblFirstName = new System.Windows.Forms.Label();
            this.TbxName = new System.Windows.Forms.TextBox();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.LblName = new System.Windows.Forms.Label();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.LblClassroom = new System.Windows.Forms.Label();
            this.CbxClasses = new System.Windows.Forms.ComboBox();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.TbxConfirmPwd = new System.Windows.Forms.TextBox();
            this.LblConfirmPwd = new System.Windows.Forms.Label();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.pnlTitleBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // LblIsTeacher
            // 
            this.LblIsTeacher.AutoSize = true;
            this.LblIsTeacher.Location = new System.Drawing.Point(47, 212);
            this.LblIsTeacher.Name = "LblIsTeacher";
            this.LblIsTeacher.Size = new System.Drawing.Size(224, 15);
            this.LblIsTeacher.TabIndex = 33;
            this.LblIsTeacher.Text = "Cette personne est-elle un enseignant ?";
            // 
            // CbxIsTeacher
            // 
            this.CbxIsTeacher.AutoSize = true;
            this.CbxIsTeacher.Location = new System.Drawing.Point(280, 212);
            this.CbxIsTeacher.Name = "CbxIsTeacher";
            this.CbxIsTeacher.Size = new System.Drawing.Size(15, 14);
            this.CbxIsTeacher.TabIndex = 32;
            this.CbxIsTeacher.UseVisualStyleBackColor = true;
            this.CbxIsTeacher.CheckedChanged += new System.EventHandler(this.CbxIsTeacher_CheckedChanged);
            // 
            // TbxPassword
            // 
            this.TbxPassword.Enabled = false;
            this.TbxPassword.Location = new System.Drawing.Point(175, 299);
            this.TbxPassword.MaxLength = 32;
            this.TbxPassword.Name = "TbxPassword";
            this.TbxPassword.Size = new System.Drawing.Size(163, 21);
            this.TbxPassword.TabIndex = 31;
            this.TbxPassword.UseSystemPasswordChar = true;
            // 
            // LblPassword
            // 
            this.LblPassword.AutoSize = true;
            this.LblPassword.Enabled = false;
            this.LblPassword.Location = new System.Drawing.Point(47, 302);
            this.LblPassword.Name = "LblPassword";
            this.LblPassword.Size = new System.Drawing.Size(83, 15);
            this.LblPassword.TabIndex = 30;
            this.LblPassword.Text = "Mot de passe*";
            // 
            // TbxLogin
            // 
            this.TbxLogin.Enabled = false;
            this.TbxLogin.Location = new System.Drawing.Point(175, 269);
            this.TbxLogin.MaxLength = 32;
            this.TbxLogin.Name = "TbxLogin";
            this.TbxLogin.Size = new System.Drawing.Size(163, 21);
            this.TbxLogin.TabIndex = 29;
            // 
            // LblLogin
            // 
            this.LblLogin.AutoSize = true;
            this.LblLogin.Enabled = false;
            this.LblLogin.Location = new System.Drawing.Point(47, 272);
            this.LblLogin.Name = "LblLogin";
            this.LblLogin.Size = new System.Drawing.Size(67, 15);
            this.LblLogin.TabIndex = 28;
            this.LblLogin.Text = "Identifiant*";
            // 
            // TbxPhone
            // 
            this.TbxPhone.Location = new System.Drawing.Point(175, 179);
            this.TbxPhone.MaxLength = 12;
            this.TbxPhone.Name = "TbxPhone";
            this.TbxPhone.Size = new System.Drawing.Size(163, 21);
            this.TbxPhone.TabIndex = 27;
            // 
            // LblPhone
            // 
            this.LblPhone.AutoSize = true;
            this.LblPhone.Location = new System.Drawing.Point(47, 182);
            this.LblPhone.Name = "LblPhone";
            this.LblPhone.Size = new System.Drawing.Size(64, 15);
            this.LblPhone.TabIndex = 26;
            this.LblPhone.Text = "Téléphone";
            // 
            // TbxEmail
            // 
            this.TbxEmail.Location = new System.Drawing.Point(175, 149);
            this.TbxEmail.MaxLength = 32;
            this.TbxEmail.Name = "TbxEmail";
            this.TbxEmail.Size = new System.Drawing.Size(163, 21);
            this.TbxEmail.TabIndex = 25;
            // 
            // LblEmail
            // 
            this.LblEmail.AutoSize = true;
            this.LblEmail.Location = new System.Drawing.Point(47, 152);
            this.LblEmail.Name = "LblEmail";
            this.LblEmail.Size = new System.Drawing.Size(42, 15);
            this.LblEmail.TabIndex = 24;
            this.LblEmail.Text = "Email*";
            // 
            // DtpBirthDate
            // 
            this.DtpBirthDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpBirthDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpBirthDate.Location = new System.Drawing.Point(175, 114);
            this.DtpBirthDate.MaxDate = new System.DateTime(2018, 11, 8, 0, 0, 0, 0);
            this.DtpBirthDate.Name = "DtpBirthDate";
            this.DtpBirthDate.Size = new System.Drawing.Size(163, 21);
            this.DtpBirthDate.TabIndex = 23;
            this.DtpBirthDate.Value = new System.DateTime(2018, 11, 8, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 15);
            this.label3.TabIndex = 22;
            this.label3.Text = "Date de naissance";
            // 
            // TbxFirstName
            // 
            this.TbxFirstName.Location = new System.Drawing.Point(175, 89);
            this.TbxFirstName.MaxLength = 32;
            this.TbxFirstName.Name = "TbxFirstName";
            this.TbxFirstName.Size = new System.Drawing.Size(163, 21);
            this.TbxFirstName.TabIndex = 21;
            // 
            // LblFirstName
            // 
            this.LblFirstName.AutoSize = true;
            this.LblFirstName.Location = new System.Drawing.Point(47, 92);
            this.LblFirstName.Name = "LblFirstName";
            this.LblFirstName.Size = new System.Drawing.Size(56, 15);
            this.LblFirstName.TabIndex = 20;
            this.LblFirstName.Text = "Prénom*";
            // 
            // TbxName
            // 
            this.TbxName.Location = new System.Drawing.Point(175, 59);
            this.TbxName.MaxLength = 32;
            this.TbxName.Name = "TbxName";
            this.TbxName.Size = new System.Drawing.Size(163, 21);
            this.TbxName.TabIndex = 19;
            // 
            // BtnAdd
            // 
            this.BtnAdd.Location = new System.Drawing.Point(251, 366);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(87, 27);
            this.BtnAdd.TabIndex = 18;
            this.BtnAdd.Text = "Ajouter";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // LblName
            // 
            this.LblName.AutoSize = true;
            this.LblName.Location = new System.Drawing.Point(47, 62);
            this.LblName.Name = "LblName";
            this.LblName.Size = new System.Drawing.Size(39, 15);
            this.LblName.TabIndex = 17;
            this.LblName.Text = "Nom*";
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.MediumAquamarine;
            this.pnlTitleBar.Controls.Add(this.lblTitle);
            this.pnlTitleBar.Controls.Add(this.btnExit);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(414, 38);
            this.pnlTitleBar.TabIndex = 34;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Montserrat SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(12, 11);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(317, 15);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "FindingMemo - Ajout ou modification d\'une personne\r\n";
            this.lblTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Montserrat SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(377, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(34, 34);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "X";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // LblClassroom
            // 
            this.LblClassroom.AutoSize = true;
            this.LblClassroom.Location = new System.Drawing.Point(47, 242);
            this.LblClassroom.Name = "LblClassroom";
            this.LblClassroom.Size = new System.Drawing.Size(45, 15);
            this.LblClassroom.TabIndex = 35;
            this.LblClassroom.Text = "Classe*";
            // 
            // CbxClasses
            // 
            this.CbxClasses.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxClasses.FormattingEnabled = true;
            this.CbxClasses.Location = new System.Drawing.Point(175, 239);
            this.CbxClasses.Name = "CbxClasses";
            this.CbxClasses.Size = new System.Drawing.Size(163, 23);
            this.CbxClasses.TabIndex = 37;
            // 
            // BtnCancel
            // 
            this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnCancel.Location = new System.Drawing.Point(50, 366);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(87, 27);
            this.BtnCancel.TabIndex = 38;
            this.BtnCancel.Text = "Annuler";
            this.BtnCancel.UseVisualStyleBackColor = true;
            // 
            // TbxConfirmPwd
            // 
            this.TbxConfirmPwd.Enabled = false;
            this.TbxConfirmPwd.Location = new System.Drawing.Point(175, 329);
            this.TbxConfirmPwd.MaxLength = 32;
            this.TbxConfirmPwd.Name = "TbxConfirmPwd";
            this.TbxConfirmPwd.Size = new System.Drawing.Size(163, 21);
            this.TbxConfirmPwd.TabIndex = 40;
            this.TbxConfirmPwd.UseSystemPasswordChar = true;
            // 
            // LblConfirmPwd
            // 
            this.LblConfirmPwd.AutoSize = true;
            this.LblConfirmPwd.Enabled = false;
            this.LblConfirmPwd.Location = new System.Drawing.Point(47, 332);
            this.LblConfirmPwd.Name = "LblConfirmPwd";
            this.LblConfirmPwd.Size = new System.Drawing.Size(130, 15);
            this.LblConfirmPwd.TabIndex = 39;
            this.LblConfirmPwd.Text = "Confirmation du mdp*";
            // 
            // BtnDelete
            // 
            this.BtnDelete.Location = new System.Drawing.Point(50, 399);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(146, 27);
            this.BtnDelete.TabIndex = 41;
            this.BtnDelete.Text = "Supprimer la personne";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Visible = false;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // FrmAddPeople
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 438);
            this.Controls.Add(this.BtnDelete);
            this.Controls.Add(this.TbxConfirmPwd);
            this.Controls.Add(this.LblConfirmPwd);
            this.Controls.Add(this.BtnCancel);
            this.Controls.Add(this.CbxClasses);
            this.Controls.Add(this.LblClassroom);
            this.Controls.Add(this.pnlTitleBar);
            this.Controls.Add(this.LblIsTeacher);
            this.Controls.Add(this.CbxIsTeacher);
            this.Controls.Add(this.TbxPassword);
            this.Controls.Add(this.LblPassword);
            this.Controls.Add(this.TbxLogin);
            this.Controls.Add(this.LblLogin);
            this.Controls.Add(this.TbxPhone);
            this.Controls.Add(this.LblPhone);
            this.Controls.Add(this.TbxEmail);
            this.Controls.Add(this.LblEmail);
            this.Controls.Add(this.DtpBirthDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TbxFirstName);
            this.Controls.Add(this.LblFirstName);
            this.Controls.Add(this.TbxName);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.LblName);
            this.Font = new System.Drawing.Font("Montserrat", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmAddPeople";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmAddPeople";
            this.Load += new System.EventHandler(this.FrmAddPeople_Load);
            this.pnlTitleBar.ResumeLayout(false);
            this.pnlTitleBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblIsTeacher;
        private System.Windows.Forms.CheckBox CbxIsTeacher;
        private System.Windows.Forms.TextBox TbxPassword;
        private System.Windows.Forms.Label LblPassword;
        private System.Windows.Forms.TextBox TbxLogin;
        private System.Windows.Forms.Label LblLogin;
        private System.Windows.Forms.TextBox TbxPhone;
        private System.Windows.Forms.Label LblPhone;
        private System.Windows.Forms.TextBox TbxEmail;
        private System.Windows.Forms.Label LblEmail;
        private System.Windows.Forms.DateTimePicker DtpBirthDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TbxFirstName;
        private System.Windows.Forms.Label LblFirstName;
        private System.Windows.Forms.TextBox TbxName;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.Label LblName;
        private System.Windows.Forms.Panel pnlTitleBar;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label LblClassroom;
        private System.Windows.Forms.ComboBox CbxClasses;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.TextBox TbxConfirmPwd;
        private System.Windows.Forms.Label LblConfirmPwd;
        private System.Windows.Forms.Button BtnDelete;
    }
}