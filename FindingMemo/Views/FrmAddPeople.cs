﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FindingMemo.Views {
    /// <summary>
    /// Form who add or edit a new people
    /// </summary>
    public partial class FrmAddPeople : Form {

        private FrmPeopleManagement _peopleManagementForm;

        private Tools _tools;

        private bool _isNewPeople = true;

        private int _idPeopleInEdition;

        /// <summary>
        /// If true then the form will be used to create a new peopl, else it will modified an existing people
        /// </summary>
        private bool IsNewPeople { get => _isNewPeople; set => _isNewPeople = value; }

        /// <summary>
        /// Id of the people we modify
        /// </summary>
        private int IdPeopleInEdition { get => _idPeopleInEdition; set => _idPeopleInEdition = value; }

        public Tools Tools { get => _tools; set => _tools = value; }

        public FrmPeopleManagement PeopleManagementForm { get => _peopleManagementForm; set => _peopleManagementForm = value; }
       

        #region Constructors
        /// <summary>
        /// Create a new form for add a new people
        /// </summary>
        public FrmAddPeople(FrmPeopleManagement frmPeopleManagement, bool isNewPeople = true) {
            InitForm(frmPeopleManagement, isNewPeople);
            BtnDelete.Visible = false;
        }

        /// <summary>
        /// Create a new form for edit a student
        /// </summary>
        public FrmAddPeople(FrmPeopleManagement frmPeopleManagement, int idPeopleInEdition, string name, string firstName, string email, DateTime? birthdate, string phone, int? idClass) : this(frmPeopleManagement, false) {
            LoadPeopleData(name, firstName, email, birthdate, phone);
            IdPeopleInEdition = idPeopleInEdition;

            CbxIsTeacher.Checked = false;
            CbxClasses.SelectedValue = idClass;
            BtnDelete.Visible = true;
        }

        /// <summary>
        /// Create a new form for edit a teacher
        /// </summary>
        public FrmAddPeople(FrmPeopleManagement frmPeopleManagement, int idPeopleInEdition, string name, string firstName, string email, DateTime? birthdate, string phone, string login, string password) : this(frmPeopleManagement, false) {
            LoadPeopleData(name, firstName, email, birthdate, phone);
            IdPeopleInEdition = idPeopleInEdition;

            CbxIsTeacher.Checked = true;
            TbxLogin.Text = login;
            TbxPassword.Text = password;
            TbxConfirmPwd.Text = password;
            BtnDelete.Visible = true;
        }

        /// <summary>
        /// Initialiaze the form
        /// </summary>
        /// <param name="frmPeopleManagement"></param>
        /// <param name="isNewPeople">Indicate if this form is gonna create a new people</param>
        private void InitForm(FrmPeopleManagement frmPeopleManagement, bool isNewPeople) {
            InitializeComponent();
            Tools = new Tools(this);
            PeopleManagementForm = frmPeopleManagement;
            IsNewPeople = isNewPeople;
            CbxIsTeacher.Enabled = IsNewPeople;
            // We must load the form to initialize components properties before loading potential data
            FrmAddPeople_Load(this, null);
        }
        #endregion Constructors

        #region Events
        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e) {
            Tools.TitleBar_MouseDown(sender, e);
        }

        private void btnExit_Click(object sender, EventArgs e) {
            this.Close();
        }
        
        private void FrmAddPeople_Load(object sender, EventArgs e) {
            // we need to save the datetime.now to avoid a potentiel crash
            DateTime now = DateTime.Now.Date;
            DtpBirthDate.MaxDate = now;
            if (IsNewPeople) {
                DtpBirthDate.Value = now;
                BtnAdd.Text = "Ajouter";
            } else {
                BtnAdd.Text = "Modifier";
            }

            // Load the classes in the combobox
            List<Classes> classes = PeopleManagementForm.MC.GetAllClasses();
            CbxClasses.DataSource = classes;
            CbxClasses.DisplayMember = "labelClass";
            CbxClasses.ValueMember = "idClass";
        }

        /// <summary>
        /// Enable and disable components compared to if the people is a teacher or student
        /// </summary>
        private void CbxIsTeacher_CheckedChanged(object sender, EventArgs e) {
            if (CbxIsTeacher.Checked) {
                LblClassroom.Enabled = false;
                CbxClasses.Enabled = false;
                LblLogin.Enabled = true;
                TbxLogin.Enabled = true;
                LblPassword.Enabled = true;
                TbxPassword.Enabled = true;
                LblConfirmPwd.Enabled = true;
                TbxConfirmPwd.Enabled = true;
            } else {
                LblClassroom.Enabled = true;
                CbxClasses.Enabled = true;
                LblLogin.Enabled = false;
                TbxLogin.Enabled = false;
                LblPassword.Enabled = false;
                TbxPassword.Enabled = false;
                LblConfirmPwd.Enabled = false;
                TbxConfirmPwd.Enabled = false;
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e) {
            // Get and trim the fields
            string name = TbxName.Text.Trim();
            string firstName = TbxFirstName.Text.Trim();
            DateTime birthDate = DtpBirthDate.Value.Date;
            string email = TbxEmail.Text.Trim();
            string phone = TbxPhone.Text.Trim();
            bool isTeacher = CbxIsTeacher.Checked;
            int idClass = (int)CbxClasses.SelectedValue;
            string login = TbxLogin.Text.Trim();
            string password = TbxPassword.Text.Trim();
            string confirmPwd = TbxConfirmPwd.Text.Trim();

            // Verify that fields are correct
            bool addOk = true;

            if (name.Length <= 0) {
                addOk = false;
            }

            if (firstName.Length <= 0) {
                addOk = false;
            }

            if (birthDate == null) {
                addOk = false;
            }

            if (email.Length <= 0) {
                addOk = false;
            }

            if (isTeacher) {
                // test specifical fields for teacher
                if (login.Length <= 0) {
                    addOk = false;
                }
                if (password.Length <= 0 && password != confirmPwd) {
                    addOk = false;
                }
                // Add a new teacher if the fields are ok
                if (addOk) {
                    if (IsNewPeople) {
                        PeopleManagementForm.MC.AddTeacher(name, firstName, birthDate, email, phone, login, password);
                    } else {
                        PeopleManagementForm.MC.EditTeacher(IdPeopleInEdition, name, firstName, birthDate, email, phone, login, password);
                        this.Close();
                    }

                    ClearFields();
                    PeopleManagementForm.ReloadPeople();
                }
            } else {
                // test specifical fields for student
                if (idClass == -1) {
                    addOk = false;
                }
                // Add a new student if the fields are ok
                if (addOk) {
                    if (IsNewPeople) {
                        PeopleManagementForm.MC.AddStudent(name, firstName, birthDate, email, phone, idClass);
                    } else {
                        PeopleManagementForm.MC.EditStudent(IdPeopleInEdition, name, firstName, birthDate, email, phone, idClass);
                        this.Close();
                    }

                    ClearFields();
                    PeopleManagementForm.ReloadPeople();
                }
            }
        }

        #endregion Events
        
        #region Fonctions
        private void LoadPeopleData(string name, string firstName, string email, DateTime? birthdate, string phone) {
            TbxName.Text = name;
            TbxFirstName.Text = firstName;
            TbxEmail.Text = email;
            TbxPhone.Text = phone;

            if (birthdate != null) {
                DtpBirthDate.Value = ((DateTime)birthdate).Date;
            }
        }

        private void ClearFields() {
            TbxName.Text = string.Empty;
            TbxFirstName.Text = string.Empty;
            TbxEmail.Text = string.Empty;
            TbxPhone.Text = string.Empty;
            TbxLogin.Text = string.Empty;
            TbxPassword.Text = string.Empty;
            TbxConfirmPwd.Text = string.Empty;
        }
        #endregion Fonctions

        private void BtnDelete_Click(object sender, EventArgs e) {
            DialogResult dr = MessageBox.Show("Vous vous apprêtez à supprimer cette personne, cette action est irréversible. Êtes-vous sûr de vouloir le faire ?", "Attention !", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (dr == DialogResult.Yes) {
                if (PeopleManagementForm.MC.DeletePeopleById(IdPeopleInEdition)) {
                    MessageBox.Show("La suppression a correctement fonctionné.");
                    PeopleManagementForm.ReloadPeople();
                    this.Close();
                } else {
                    MessageBox.Show("Une erreure est survenue lors de la suppression.");
                }
            }
        }
    }
}
