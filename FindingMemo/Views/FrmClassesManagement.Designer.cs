﻿namespace FindingMemo.Views
{
    partial class FrmClassesManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.findingMemoDBDataSet = new FindingMemo.FindingMemoDBDataSet();
            this.classesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.classesTableAdapter = new FindingMemo.FindingMemoDBDataSetTableAdapters.ClassesTableAdapter();
            this.tableAdapterManager = new FindingMemo.FindingMemoDBDataSetTableAdapters.TableAdapterManager();
            this.classesDataGridView = new System.Windows.Forms.DataGridView();
            this.clmnIdClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnLabelClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnCapacity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnAddClass = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gbxAddClass = new System.Windows.Forms.GroupBox();
            this.lblCapacity = new System.Windows.Forms.Label();
            this.lblClassName = new System.Windows.Forms.Label();
            this.tbxClassName = new System.Windows.Forms.TextBox();
            this.btnSaveClass = new System.Windows.Forms.Button();
            this.nudCapacity = new System.Windows.Forms.NumericUpDown();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.findingMemoDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.classesDataGridView)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gbxAddClass.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCapacity)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(593, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(36, 32);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "X";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Montserrat SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(10, 12);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(211, 15);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "FindingMemo - Gestion des classes";
            this.lblTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseDown);
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.MediumAquamarine;
            this.pnlTitleBar.Controls.Add(this.btnExit);
            this.pnlTitleBar.Controls.Add(this.lblTitle);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(632, 38);
            this.pnlTitleBar.TabIndex = 2;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseDown);
            // 
            // findingMemoDBDataSet
            // 
            this.findingMemoDBDataSet.DataSetName = "FindingMemoDBDataSet";
            this.findingMemoDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // classesBindingSource
            // 
            this.classesBindingSource.DataMember = "Classes";
            this.classesBindingSource.DataSource = this.findingMemoDBDataSet;
            // 
            // classesTableAdapter
            // 
            this.classesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ClassesTableAdapter = this.classesTableAdapter;
            this.tableAdapterManager.LessonsTableAdapter = null;
            this.tableAdapterManager.PeopleTableAdapter = null;
            this.tableAdapterManager.PlanningsTableAdapter = null;
            this.tableAdapterManager.PresencesTableAdapter = null;
            this.tableAdapterManager.StatusTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = FindingMemo.FindingMemoDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // classesDataGridView
            // 
            this.classesDataGridView.AllowUserToAddRows = false;
            this.classesDataGridView.AllowUserToDeleteRows = false;
            this.classesDataGridView.AllowUserToResizeColumns = false;
            this.classesDataGridView.AllowUserToResizeRows = false;
            this.classesDataGridView.AutoGenerateColumns = false;
            this.classesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.classesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.classesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmnIdClass,
            this.clmnLabelClass,
            this.clmnCapacity});
            this.classesDataGridView.DataSource = this.classesBindingSource;
            this.classesDataGridView.Location = new System.Drawing.Point(14, 17);
            this.classesDataGridView.Name = "classesDataGridView";
            this.classesDataGridView.ReadOnly = true;
            this.classesDataGridView.RowHeadersVisible = false;
            this.classesDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.classesDataGridView.Size = new System.Drawing.Size(376, 166);
            this.classesDataGridView.TabIndex = 3;
            this.classesDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.classesDataGridView_CellClick);
            // 
            // clmnIdClass
            // 
            this.clmnIdClass.DataPropertyName = "idClass";
            this.clmnIdClass.HeaderText = "idClass";
            this.clmnIdClass.Name = "clmnIdClass";
            this.clmnIdClass.ReadOnly = true;
            this.clmnIdClass.Visible = false;
            // 
            // clmnLabelClass
            // 
            this.clmnLabelClass.DataPropertyName = "labelClass";
            this.clmnLabelClass.HeaderText = "labelClass";
            this.clmnLabelClass.Name = "clmnLabelClass";
            this.clmnLabelClass.ReadOnly = true;
            // 
            // clmnCapacity
            // 
            this.clmnCapacity.DataPropertyName = "capacity";
            this.clmnCapacity.HeaderText = "capacity";
            this.clmnCapacity.Name = "clmnCapacity";
            this.clmnCapacity.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnAddClass);
            this.panel2.Controls.Add(this.classesDataGridView);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 38);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(404, 225);
            this.panel2.TabIndex = 5;
            // 
            // btnAddClass
            // 
            this.btnAddClass.Enabled = false;
            this.btnAddClass.Location = new System.Drawing.Point(14, 190);
            this.btnAddClass.Name = "btnAddClass";
            this.btnAddClass.Size = new System.Drawing.Size(75, 23);
            this.btnAddClass.TabIndex = 4;
            this.btnAddClass.Text = "Ajouter";
            this.btnAddClass.UseVisualStyleBackColor = true;
            this.btnAddClass.Click += new System.EventHandler(this.btnAddClass_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.gbxAddClass);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(404, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(228, 225);
            this.panel1.TabIndex = 6;
            // 
            // gbxAddClass
            // 
            this.gbxAddClass.Controls.Add(this.lblCapacity);
            this.gbxAddClass.Controls.Add(this.lblClassName);
            this.gbxAddClass.Controls.Add(this.tbxClassName);
            this.gbxAddClass.Controls.Add(this.btnSaveClass);
            this.gbxAddClass.Controls.Add(this.nudCapacity);
            this.gbxAddClass.Location = new System.Drawing.Point(7, 17);
            this.gbxAddClass.Name = "gbxAddClass";
            this.gbxAddClass.Size = new System.Drawing.Size(210, 166);
            this.gbxAddClass.TabIndex = 4;
            this.gbxAddClass.TabStop = false;
            this.gbxAddClass.Text = "Ajouter une classe";
            // 
            // lblCapacity
            // 
            this.lblCapacity.AutoSize = true;
            this.lblCapacity.Location = new System.Drawing.Point(17, 54);
            this.lblCapacity.Name = "lblCapacity";
            this.lblCapacity.Size = new System.Drawing.Size(54, 15);
            this.lblCapacity.TabIndex = 6;
            this.lblCapacity.Text = "Capacité";
            // 
            // lblClassName
            // 
            this.lblClassName.AutoSize = true;
            this.lblClassName.Location = new System.Drawing.Point(17, 25);
            this.lblClassName.Name = "lblClassName";
            this.lblClassName.Size = new System.Drawing.Size(35, 15);
            this.lblClassName.TabIndex = 5;
            this.lblClassName.Text = "Nom";
            // 
            // tbxClassName
            // 
            this.tbxClassName.Location = new System.Drawing.Point(58, 22);
            this.tbxClassName.Name = "tbxClassName";
            this.tbxClassName.Size = new System.Drawing.Size(144, 21);
            this.tbxClassName.TabIndex = 1;
            this.tbxClassName.TextChanged += new System.EventHandler(this.tbxClassName_TextChanged);
            // 
            // btnSaveClass
            // 
            this.btnSaveClass.Enabled = false;
            this.btnSaveClass.Location = new System.Drawing.Point(7, 133);
            this.btnSaveClass.Name = "btnSaveClass";
            this.btnSaveClass.Size = new System.Drawing.Size(196, 27);
            this.btnSaveClass.TabIndex = 0;
            this.btnSaveClass.Text = "Sauvegarder";
            this.btnSaveClass.UseVisualStyleBackColor = true;
            this.btnSaveClass.Click += new System.EventHandler(this.btnSaveClass_Click);
            // 
            // nudCapacity
            // 
            this.nudCapacity.Location = new System.Drawing.Point(155, 52);
            this.nudCapacity.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudCapacity.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudCapacity.Name = "nudCapacity";
            this.nudCapacity.Size = new System.Drawing.Size(48, 21);
            this.nudCapacity.TabIndex = 2;
            this.nudCapacity.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // FrmClassesManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 263);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Montserrat", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmClassesManagement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmEditClasses";
            this.Load += new System.EventHandler(this.FrmEditClasses_Load);
            this.pnlTitleBar.ResumeLayout(false);
            this.pnlTitleBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.findingMemoDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.classesDataGridView)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.gbxAddClass.ResumeLayout(false);
            this.gbxAddClass.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCapacity)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel pnlTitleBar;
        private FindingMemoDBDataSet findingMemoDBDataSet;
        private System.Windows.Forms.BindingSource classesBindingSource;
        private FindingMemoDBDataSetTableAdapters.ClassesTableAdapter classesTableAdapter;
        private FindingMemoDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView classesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnIdClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnLabelClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnCapacity;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown nudCapacity;
        private System.Windows.Forms.TextBox tbxClassName;
        private System.Windows.Forms.Button btnSaveClass;
        private System.Windows.Forms.GroupBox gbxAddClass;
        private System.Windows.Forms.Label lblCapacity;
        private System.Windows.Forms.Label lblClassName;
        private System.Windows.Forms.Button btnAddClass;
    }
}