﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using FindingMemo.Controllers;
using System;
using System.Windows.Forms;

namespace FindingMemo.Views {
    public partial class FrmClassesManagement : Form
    {
        private MemoController _mC;
        private Tools _t;
        private FrmPeopleManagement _frm;
        private int _currentEditedClassId;
        //Mode d'ajout si true et d'édition si false
        private bool _addMode = true;
        public MemoController MC { get => _mC; set => _mC = value; }
        public Tools T { get => _t; set => _t = value; }
        public FrmPeopleManagement Frm { get => _frm; set => _frm = value; }
        public int CurrentEditedClassId { get => _currentEditedClassId; set => _currentEditedClassId = value; }
        public bool AddMode { get => _addMode; set => _addMode = value; }

        public FrmClassesManagement(FrmPeopleManagement frm)
        {
            InitializeComponent();
            Frm = frm;
            T = new Tools(this);
            MC = Frm.MC;
        }

        private void classesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.classesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.findingMemoDBDataSet);

        }

        private void FrmEditClasses_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'findingMemoDBDataSet.Classes'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.classesTableAdapter.Fill(this.findingMemoDBDataSet.Classes);

        }

        private void tbxClassName_TextChanged(object sender, EventArgs e)
        {
            btnSaveClass.Enabled = tbxClassName.Text != string.Empty;
        }

        private void btnAddClass_Click(object sender, EventArgs e)
        {
            AddMode = true;
            UpdateDisplayByMode();
        }

        private void UpdateDisplayByMode()
        {
            btnAddClass.Enabled = !AddMode;
            gbxAddClass.Text = String.Format("{0} une classe", AddMode ? "Ajouter" : "Modifier");
            tbxClassName.Clear();
            nudCapacity.Value = 10;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            T.TitleBar_MouseDown(sender, e);
        }

        private void btnSaveClass_Click(object sender, EventArgs e)
        {
            //On appelle la méthode pour ajouter une nouvelle classe ou modifier en fonction du mode d'édition
            if (AddMode ? MC.AddClass(tbxClassName.Text, (int)nudCapacity.Value) : MC.EditClass(CurrentEditedClassId, tbxClassName.Text, (int)nudCapacity.Value))
            {
                classesTableAdapter.Fill(findingMemoDBDataSet.Classes);
                
                AddMode = true;
                UpdateDisplayByMode();
            }

        }

        private void classesDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            AddMode = false;
            UpdateDisplayByMode();
            if (e.ColumnIndex == 1)
            {
                int idClass = (int)classesDataGridView.Rows[e.RowIndex].Cells[0].Value;
                Classes c = MC.GetClassesById(idClass);
                CurrentEditedClassId = c.idClass;
                tbxClassName.Text = c.labelClass;
                nudCapacity.Value = c.capacity;
            }
        }
    }
}
