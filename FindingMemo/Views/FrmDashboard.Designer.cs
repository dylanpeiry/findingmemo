﻿namespace FindingMemo.Views
{
    partial class FrmDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlProfile = new System.Windows.Forms.Panel();
            this.btnHelp = new System.Windows.Forms.Button();
            this.pnlActions = new System.Windows.Forms.Panel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.lblLogin = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.profileBorder = new System.Windows.Forms.Label();
            this.pnlDashboard = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnFillWithSelectedMode = new System.Windows.Forms.Button();
            this.btnSavePresences = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnRelease = new System.Windows.Forms.Button();
            this.btnPresent = new System.Windows.Forms.Button();
            this.btnAway = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvPresences = new System.Windows.Forms.DataGridView();
            this.clmnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnLastFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmnIdStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbxLessons = new System.Windows.Forms.ComboBox();
            this.dtpLessons = new System.Windows.Forms.DateTimePicker();
            this.cbxClasses = new System.Windows.Forms.ComboBox();
            this.tlTipPassword = new System.Windows.Forms.ToolTip(this.components);
            this.pnlTitleBar.SuspendLayout();
            this.pnlProfile.SuspendLayout();
            this.pnlActions.SuspendLayout();
            this.pnlInfo.SuspendLayout();
            this.pnlDashboard.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPresences)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.Salmon;
            this.pnlTitleBar.Controls.Add(this.lblTitle);
            this.pnlTitleBar.Controls.Add(this.btnExit);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(866, 38);
            this.pnlTitleBar.TabIndex = 0;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseDown);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Montserrat SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(9, 12);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(164, 15);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "FindingMemo - Dashboard";
            this.lblTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseDown);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(831, 5);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(31, 28);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "X";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlProfile
            // 
            this.pnlProfile.Controls.Add(this.btnHelp);
            this.pnlProfile.Controls.Add(this.pnlActions);
            this.pnlProfile.Controls.Add(this.pnlInfo);
            this.pnlProfile.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlProfile.Location = new System.Drawing.Point(608, 38);
            this.pnlProfile.Name = "pnlProfile";
            this.pnlProfile.Size = new System.Drawing.Size(258, 518);
            this.pnlProfile.TabIndex = 1;
            // 
            // btnHelp
            // 
            this.btnHelp.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHelp.Location = new System.Drawing.Point(14, 212);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(229, 25);
            this.btnHelp.TabIndex = 3;
            this.btnHelp.Text = "Aide";
            this.btnHelp.UseVisualStyleBackColor = true;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // pnlActions
            // 
            this.pnlActions.Controls.Add(this.btnLogout);
            this.pnlActions.Location = new System.Drawing.Point(7, 463);
            this.pnlActions.Name = "pnlActions";
            this.pnlActions.Size = new System.Drawing.Size(244, 46);
            this.pnlActions.TabIndex = 3;
            // 
            // btnLogout
            // 
            this.btnLogout.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.Location = new System.Drawing.Point(7, 10);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(230, 27);
            this.btnLogout.TabIndex = 0;
            this.btnLogout.Text = "Déconnexion";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // pnlInfo
            // 
            this.pnlInfo.BackColor = System.Drawing.Color.White;
            this.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlInfo.Controls.Add(this.lblDate);
            this.pnlInfo.Controls.Add(this.lblId);
            this.pnlInfo.Controls.Add(this.lblLogin);
            this.pnlInfo.Controls.Add(this.lblEmail);
            this.pnlInfo.Controls.Add(this.lblInfo);
            this.pnlInfo.Location = new System.Drawing.Point(7, 52);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(243, 154);
            this.pnlInfo.TabIndex = 2;
            // 
            // lblDate
            // 
            this.lblDate.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(6, 54);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(230, 24);
            this.lblDate.TabIndex = 4;
            this.lblDate.Text = "USERBIRTHDATE";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblId
            // 
            this.lblId.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId.Location = new System.Drawing.Point(6, 96);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(230, 24);
            this.lblId.TabIndex = 3;
            this.lblId.Text = "Identifiant";
            this.lblId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLogin
            // 
            this.lblLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLogin.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin.ForeColor = System.Drawing.Color.Red;
            this.lblLogin.Location = new System.Drawing.Point(6, 120);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(229, 24);
            this.lblLogin.TabIndex = 2;
            this.lblLogin.Text = "USERLOGIN";
            this.lblLogin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblLogin.Click += new System.EventHandler(this.lblLogin_Click);
            this.lblLogin.MouseEnter += new System.EventHandler(this.lblLogin_MouseEnter);
            this.lblLogin.MouseLeave += new System.EventHandler(this.lblLogin_MouseLeave);
            // 
            // lblEmail
            // 
            this.lblEmail.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(6, 30);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(230, 24);
            this.lblEmail.TabIndex = 1;
            this.lblEmail.Text = "USERMAIL";
            this.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblInfo
            // 
            this.lblInfo.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.Location = new System.Drawing.Point(6, 6);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(230, 24);
            this.lblInfo.TabIndex = 0;
            this.lblInfo.Text = "USERNAME";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // profileBorder
            // 
            this.profileBorder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.profileBorder.Location = new System.Drawing.Point(608, 38);
            this.profileBorder.Name = "profileBorder";
            this.profileBorder.Size = new System.Drawing.Size(1, 560);
            this.profileBorder.TabIndex = 2;
            // 
            // pnlDashboard
            // 
            this.pnlDashboard.Controls.Add(this.panel4);
            this.pnlDashboard.Controls.Add(this.panel3);
            this.pnlDashboard.Controls.Add(this.panel2);
            this.pnlDashboard.Controls.Add(this.panel1);
            this.pnlDashboard.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlDashboard.Location = new System.Drawing.Point(0, 38);
            this.pnlDashboard.Name = "pnlDashboard";
            this.pnlDashboard.Size = new System.Drawing.Size(609, 518);
            this.pnlDashboard.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnFillWithSelectedMode);
            this.panel4.Controls.Add(this.btnSavePresences);
            this.panel4.Location = new System.Drawing.Point(381, 149);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(211, 360);
            this.panel4.TabIndex = 3;
            // 
            // btnFillWithPresent
            // 
            this.btnFillWithSelectedMode.Enabled = false;
            this.btnFillWithSelectedMode.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFillWithSelectedMode.Location = new System.Drawing.Point(4, 3);
            this.btnFillWithSelectedMode.Name = "btnFillWithPresent";
            this.btnFillWithSelectedMode.Size = new System.Drawing.Size(204, 45);
            this.btnFillWithSelectedMode.TabIndex = 1;
            this.btnFillWithSelectedMode.Text = "Remplir les champs vides avec Présent";
            this.btnFillWithSelectedMode.UseVisualStyleBackColor = true;
            this.btnFillWithSelectedMode.Click += new System.EventHandler(this.btnFillWithSelectedMode_Click);
            // 
            // btnSavePresences
            // 
            this.btnSavePresences.Enabled = false;
            this.btnSavePresences.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSavePresences.Location = new System.Drawing.Point(4, 325);
            this.btnSavePresences.Name = "btnSavePresences";
            this.btnSavePresences.Size = new System.Drawing.Size(204, 25);
            this.btnSavePresences.TabIndex = 2;
            this.btnSavePresences.Text = "Sauvegarder";
            this.btnSavePresences.UseVisualStyleBackColor = true;
            this.btnSavePresences.Click += new System.EventHandler(this.btnSavePresences_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnRelease);
            this.panel3.Controls.Add(this.btnPresent);
            this.panel3.Controls.Add(this.btnAway);
            this.panel3.Location = new System.Drawing.Point(14, 98);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(579, 40);
            this.panel3.TabIndex = 2;
            // 
            // btnRelease
            // 
            this.btnRelease.BackColor = System.Drawing.Color.SkyBlue;
            this.btnRelease.FlatAppearance.BorderSize = 0;
            this.btnRelease.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRelease.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRelease.Location = new System.Drawing.Point(161, 5);
            this.btnRelease.Name = "btnRelease";
            this.btnRelease.Size = new System.Drawing.Size(71, 30);
            this.btnRelease.TabIndex = 2;
            this.btnRelease.Text = "Libéré";
            this.btnRelease.UseVisualStyleBackColor = false;
            this.btnRelease.Click += new System.EventHandler(this.PresenceMode_Click);
            // 
            // btnPresent
            // 
            this.btnPresent.BackColor = System.Drawing.Color.LightGreen;
            this.btnPresent.FlatAppearance.BorderSize = 0;
            this.btnPresent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPresent.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPresent.Location = new System.Drawing.Point(5, 5);
            this.btnPresent.Name = "btnPresent";
            this.btnPresent.Size = new System.Drawing.Size(71, 30);
            this.btnPresent.TabIndex = 1;
            this.btnPresent.Text = "Présent";
            this.btnPresent.UseVisualStyleBackColor = false;
            this.btnPresent.Click += new System.EventHandler(this.PresenceMode_Click);
            // 
            // btnAway
            // 
            this.btnAway.BackColor = System.Drawing.Color.IndianRed;
            this.btnAway.FlatAppearance.BorderSize = 0;
            this.btnAway.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAway.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAway.Location = new System.Drawing.Point(83, 5);
            this.btnAway.Name = "btnAway";
            this.btnAway.Size = new System.Drawing.Size(71, 30);
            this.btnAway.TabIndex = 0;
            this.btnAway.Text = "Absent";
            this.btnAway.UseVisualStyleBackColor = false;
            this.btnAway.Click += new System.EventHandler(this.PresenceMode_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvPresences);
            this.panel2.Location = new System.Drawing.Point(13, 149);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(359, 360);
            this.panel2.TabIndex = 1;
            // 
            // dgvPresences
            // 
            this.dgvPresences.AllowUserToAddRows = false;
            this.dgvPresences.AllowUserToDeleteRows = false;
            this.dgvPresences.AllowUserToResizeColumns = false;
            this.dgvPresences.AllowUserToResizeRows = false;
            this.dgvPresences.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPresences.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPresences.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmnId,
            this.clmnLastFirstName,
            this.clmnStatus,
            this.clmnIdStatus});
            this.dgvPresences.Location = new System.Drawing.Point(3, 3);
            this.dgvPresences.Name = "dgvPresences";
            this.dgvPresences.RowHeadersVisible = false;
            this.dgvPresences.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvPresences.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvPresences.Size = new System.Drawing.Size(352, 352);
            this.dgvPresences.TabIndex = 0;
            this.dgvPresences.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPresences_CellClick);
            this.dgvPresences.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPresences_CellDoubleClick);
            this.dgvPresences.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPresences_CellValueChanged);
            // 
            // clmnId
            // 
            this.clmnId.HeaderText = "Id";
            this.clmnId.Name = "clmnId";
            this.clmnId.Visible = false;
            // 
            // clmnLastFirstName
            // 
            this.clmnLastFirstName.FillWeight = 131.9797F;
            this.clmnLastFirstName.HeaderText = "Élève (Nom/Prénom)";
            this.clmnLastFirstName.Name = "clmnLastFirstName";
            this.clmnLastFirstName.ReadOnly = true;
            // 
            // clmnStatus
            // 
            this.clmnStatus.FillWeight = 68.02031F;
            this.clmnStatus.HeaderText = "Statut";
            this.clmnStatus.Name = "clmnStatus";
            this.clmnStatus.ReadOnly = true;
            this.clmnStatus.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // clmnIdStatus
            // 
            this.clmnIdStatus.HeaderText = "Id statut";
            this.clmnIdStatus.Name = "clmnIdStatus";
            this.clmnIdStatus.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbxLessons);
            this.panel1.Controls.Add(this.dtpLessons);
            this.panel1.Controls.Add(this.cbxClasses);
            this.panel1.Location = new System.Drawing.Point(14, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(579, 38);
            this.panel1.TabIndex = 0;
            // 
            // cbxLessons
            // 
            this.cbxLessons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxLessons.FormattingEnabled = true;
            this.cbxLessons.Location = new System.Drawing.Point(439, 7);
            this.cbxLessons.Name = "cbxLessons";
            this.cbxLessons.Size = new System.Drawing.Size(136, 23);
            this.cbxLessons.TabIndex = 3;
            this.cbxLessons.SelectedIndexChanged += new System.EventHandler(this.cbxLessons_SelectedIndexChanged);
            // 
            // dtpLessons
            // 
            this.dtpLessons.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpLessons.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpLessons.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpLessons.Location = new System.Drawing.Point(5, 7);
            this.dtpLessons.MaxDate = new System.DateTime(2018, 11, 8, 0, 0, 0, 0);
            this.dtpLessons.Name = "dtpLessons";
            this.dtpLessons.Size = new System.Drawing.Size(163, 21);
            this.dtpLessons.TabIndex = 2;
            this.dtpLessons.Value = new System.DateTime(2018, 11, 8, 0, 0, 0, 0);
            this.dtpLessons.ValueChanged += new System.EventHandler(this.dtpLessons_ValueChanged);
            // 
            // cbxClasses
            // 
            this.cbxClasses.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxClasses.FormattingEnabled = true;
            this.cbxClasses.Location = new System.Drawing.Point(222, 7);
            this.cbxClasses.Name = "cbxClasses";
            this.cbxClasses.Size = new System.Drawing.Size(136, 23);
            this.cbxClasses.TabIndex = 1;
            this.cbxClasses.SelectedIndexChanged += new System.EventHandler(this.cbxClasses_SelectedIndexChanged);
            // 
            // tlTipPassword
            // 
            this.tlTipPassword.ToolTipTitle = "Informations";
            // 
            // FrmDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 556);
            this.Controls.Add(this.profileBorder);
            this.Controls.Add(this.pnlDashboard);
            this.Controls.Add(this.pnlProfile);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Montserrat", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "  ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmDashboard_FormClosing);
            this.Load += new System.EventHandler(this.FrmDashboard_Load);
            this.pnlTitleBar.ResumeLayout(false);
            this.pnlTitleBar.PerformLayout();
            this.pnlProfile.ResumeLayout(false);
            this.pnlActions.ResumeLayout(false);
            this.pnlInfo.ResumeLayout(false);
            this.pnlDashboard.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPresences)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTitleBar;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Panel pnlProfile;
        private System.Windows.Forms.Panel pnlActions;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Label profileBorder;
        private System.Windows.Forms.Panel pnlDashboard;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cbxLessons;
        private System.Windows.Forms.DateTimePicker dtpLessons;
        private System.Windows.Forms.ComboBox cbxClasses;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnPresent;
        private System.Windows.Forms.Button btnAway;
        private System.Windows.Forms.DataGridView dgvPresences;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnFillWithSelectedMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnId;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnLastFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmnIdStatus;
        private System.Windows.Forms.Button btnSavePresences;
        private System.Windows.Forms.Button btnRelease;
        private System.Windows.Forms.ToolTip tlTipPassword;
        private System.Windows.Forms.Button btnHelp;
    }
}