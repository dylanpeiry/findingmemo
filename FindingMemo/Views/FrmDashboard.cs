﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using FindingMemo.Controllers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace FindingMemo.Views {
    public partial class FrmDashboard : Form
    {
        private const int COL_IDPEOPLE = 0;
        private const int COL_NAME = 1;
        private const int COL_STATUS = 2;
        private const int COL_IDSTATUS = 3;

        private Tools _t;
        private FrmLogin _login;
        private int _idLesson;
        private int _idClass;
        private DateTime _now;
        private Dictionary<int, int> _currentStudentsPresences;


        private Tools T { get => _t; set => _t = value; }
        private FrmLogin Login { get => _login; set => _login = value; }

        private MemoController MC { get => Login.MC; set => Login.MC = value; }
        public int IdLesson { get => _idLesson; set => _idLesson = value; }
        public int IdClass { get => _idClass; set => _idClass = value; }
        public DateTime Now { get => _now; set => _now = value; }
        public Dictionary<int, int> CurrentStudentsPresences { get => _currentStudentsPresences; set => _currentStudentsPresences = value; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="frmLogin"></param>
        public FrmDashboard(FrmLogin frmLogin)
        {
            InitializeComponent();
            T = new Tools(this);
            Login = frmLogin;
            Login.Visible = false;
            CurrentStudentsPresences = new Dictionary<int, int>();
            /* Initialise la date du calendrier à aujourd'hui*/
            Now = DateTime.Now;
            dtpLessons.MaxDate = Now;
            dtpLessons.Value = Now;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            T.TitleBar_MouseDown(sender, e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmDashboard_Load(object sender, EventArgs e)
        {
            /* Affichage des informations du profil */
            lblInfo.Text = MC.GetNameConnectedUser();
            lblEmail.Text = MC.GetEmailConnectedUser();
            lblDate.Text = MC.GetBirthDateConnectedUser();
            lblLogin.Text = MC.GetLoginConnectedUser();

            /* Génération des listes déroulantes par défaut lors du lancement de l'app*/
            DateTime dateOfLesson = dtpLessons.Value;
            int d = (int)dateOfLesson.DayOfWeek;


            Dictionary<int, string> classes = MC.GetClassesByTeacher(MC.GetIdConnectedUser());
            cbxClasses.DataSource = new BindingSource(classes, null);
            cbxClasses.DisplayMember = "Value";
            cbxClasses.ValueMember = "Key";
            IdClass = ((KeyValuePair<int, string>)cbxClasses.SelectedItem).Key;

            if (UpdateLessonsComboBox(d) != -1)
            {
                IdLesson = ((KeyValuePair<int, string>)cbxLessons.SelectedItem).Key;
                DisplayStudents(dateOfLesson, IdLesson, IdClass);
            }
            ShowCurrentPresenceMode();
            tlTipPassword.SetToolTip(lblLogin, "Cliquez ici pour modifier votre mot de passe");


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateOfLesson"></param>
        /// <param name="idLesson"></param>
        /// <param name="idClass"></param>
        private void DisplayStudents(DateTime dateOfLesson, int idLesson, int idClass)
        {
            dgvPresences.Rows.Clear();
            CurrentStudentsPresences.Clear();

            if (idLesson > 0)
            {
                Dictionary<int, string> students = MC.GetStudentsByClass(idClass);

                foreach (KeyValuePair<int, string> item in students)
                {
                    MemoController.PresenceMode status = MC.GetUserStatusId(item.Key, idLesson, dateOfLesson);
                    if ((int)status == 0)
                    {
                        dgvPresences.Rows.Add(item.Key, item.Value);

                    }
                    else
                    {
                        dgvPresences.Rows.Add(item.Key, item.Value, MC.GetEnumString(status), (int)status);
                        dgvPresences.Rows[dgvPresences.Rows.Count - 1].Cells[COL_STATUS].Style.BackColor = MC.PresenceModeProperties[status];
                        CurrentStudentsPresences.Add(item.Key, (int)status);
                    }
                }
                btnFillWithSelectedMode.Enabled = true;

            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ShowCurrentPresenceMode()
        {
            switch (MC.CurrentPresenceMode)
            {
                case MemoController.PresenceMode.Présent:
                    btnPresent.Font = new Font(btnPresent.Font, FontStyle.Bold);
                    break;
                case MemoController.PresenceMode.Absent:
                    btnAway.Font = new Font(btnAway.Font, FontStyle.Bold);
                    break;
                case MemoController.PresenceMode.Libéré:
                    btnRelease.Font = new Font(btnRelease.Font, FontStyle.Bold);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PresenceMode_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            switch (btn.Name)
            {
                case "btnAway":
                    UpdateDisplayedPresenceMode(MemoController.PresenceMode.Absent);
                    break;
                case "btnPresent":
                    UpdateDisplayedPresenceMode(MemoController.PresenceMode.Présent);
                    break;
                case "btnRelease":
                    UpdateDisplayedPresenceMode(MemoController.PresenceMode.Libéré);
                    break;
                default:
                    break;
            }
            btnFillWithSelectedMode.Text = String.Format("Remplir les champs vides avec {0}",MC.CurrentPresenceMode);
        }

        /// <summary>
        /// Met à jour le mode de présence
        /// </summary>
        /// <param name="p"></param>
        private void UpdateDisplayedPresenceMode(MemoController.PresenceMode p)
        {
            btnAway.Font = new Font(btnAway.Font, FontStyle.Regular);
            btnPresent.Font = new Font(btnPresent.Font, FontStyle.Regular);
            btnRelease.Font = new Font(btnRelease.Font, FontStyle.Regular);
            switch (p)
            {
                case MemoController.PresenceMode.Présent:
                    btnPresent.Font = new Font(btnPresent.Font, FontStyle.Bold);
                    break;
                case MemoController.PresenceMode.Absent:
                    btnAway.Font = new Font(btnAway.Font, FontStyle.Bold);
                    break;
                case MemoController.PresenceMode.Libéré:
                    btnRelease.Font = new Font(btnRelease.Font, FontStyle.Bold);
                    break;
                default:
                    break;
            }
            MC.CurrentPresenceMode = p;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogout_Click(object sender, EventArgs e)
        {
            Close();
            Login.Visible = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmDashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            T.PromptExit(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Actualise la liste des élèves affichés par rapport au cours et à la classe
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtpLessons_ValueChanged(object sender, EventArgs e)
        {
            if (cbxClasses.Items.Count > 0 && cbxLessons.Items.Count > 0)
            {
                btnFillWithSelectedMode.Enabled = false;
                int idClass = ((KeyValuePair<int, string>)cbxClasses.SelectedItem).Key;
                UpdateLessonsComboBox((int)dtpLessons.Value.DayOfWeek);
            }


        }

        /// <summary>
        /// Lors d'un clic sur une cellule de la column statut, affiche le texte et la couleur correspondant au mode de présence actif
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvPresences_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewTextBoxColumn && e.RowIndex >= 0 && e.ColumnIndex == 2)
            {
                DataGridViewTextBoxCell tbxStatus = (DataGridViewTextBoxCell)senderGrid.Rows[e.RowIndex].Cells[COL_STATUS];
                DataGridViewTextBoxCell tbxIdStatus = (DataGridViewTextBoxCell)senderGrid.Rows[e.RowIndex].Cells[COL_IDSTATUS];
                SetStatusToFields(tbxStatus, tbxIdStatus, MC.CurrentPresenceMode);
            }
        }

        /// <summary>
        /// Remplis tous les champs vides avec le mode sélectionné
        /// </summary>
        private void SetBlankFields()
        {
            DataGridViewTextBoxCell tbxStatus;
            DataGridViewTextBoxCell tbxIdStatus;
            for (int i = 0; i < dgvPresences.RowCount; i++)
            {
                tbxStatus = (DataGridViewTextBoxCell)dgvPresences[2, i];
                tbxIdStatus = (DataGridViewTextBoxCell)dgvPresences[3, i];

                if (tbxIdStatus.Value == null)
                {
                    MemoController.PresenceMode presenceMode = MC.CurrentPresenceMode;
                    SetStatusToFields(tbxStatus, tbxIdStatus, presenceMode);
                }

            }
        }


        /// <summary>
        /// Applique le mode de présence actif à tous les champs vides de la colonne "statut"
        /// </summary>
        /// <param name="tbxStatus"></param>
        /// <param name="tbxIdStatus"></param>
        /// <param name="presenceMode"></param>
        private void SetStatusToFields(DataGridViewTextBoxCell tbxStatus, DataGridViewTextBoxCell tbxIdStatus, MemoController.PresenceMode presenceMode)
        {
            tbxIdStatus.Value = (int)presenceMode;
            tbxStatus.Value = MC.GetEnumString(presenceMode);
            tbxStatus.Style.BackColor = MC.PresenceModeProperties[presenceMode];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFillWithSelectedMode_Click(object sender, EventArgs e)
        {
            SetBlankFields();
            btnSavePresences.Enabled = true;
            btnFillWithSelectedMode.Enabled = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvPresences_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            bool isACellEmpty = true;
            foreach (DataGridViewRow row in dgvPresences.Rows)
            {
                DataGridViewCell c = row.Cells[COL_STATUS];
                isACellEmpty = (c.Value == null);
                if (isACellEmpty)
                    return;
            }
            btnSavePresences.Enabled = !isACellEmpty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSavePresences_Click(object sender, EventArgs e)
        {
            Dictionary<int, int> newStudentsPresences = new Dictionary<int, int>();
            if (CurrentStudentsPresences.Count > 0)
            {
                foreach (DataGridViewRow row in dgvPresences.Rows)
                {
                    int idPeople = (int)row.Cells[COL_IDPEOPLE].Value;
                    int idStatus = (int)row.Cells[COL_IDSTATUS].Value;

                    newStudentsPresences.Add(idPeople, idStatus);
                }
                var presencesToEdit = CurrentStudentsPresences.Where(entry => newStudentsPresences[entry.Key] != entry.Value)
                    .ToDictionary(entry => entry.Key, entry => newStudentsPresences[entry.Key]);

                string message;
                if (MC.UpdatePresences(presencesToEdit, dtpLessons.Value, IdLesson))
                {
                    message = string.Format("Les modifications ont été effectuées avec succès!");
                }
                else
                {
                    message = string.Format("Une erreur est survenue, toutes les modifications n'ont pas été effectuées.");
                }
                MessageBox.Show(message,"Informations");

            }
            else
            {
                bool anErrorOccured = false;
                foreach (DataGridViewRow row in dgvPresences.Rows)
                {
                    int idPeople = (int)row.Cells[COL_IDPEOPLE].Value;
                    int idStatus = (int)row.Cells[COL_IDSTATUS].Value;

                    if (!MC.SavePresence(idPeople, IdLesson, idStatus, dtpLessons.Value))
                    {
                        //Si une des présence n'est pas enregistrée, on informe l'utilisateur d'une erreur
                        anErrorOccured = true;
                        return;
                    }
                }
                if (anErrorOccured)
                {
                    MessageBox.Show("Les présences n'ont pas pu être enregistrées, une erreur est survenue.","Erreur");
                }
                else
                {
                    MessageBox.Show("Les présences ont été enregistrées avec succès!", "Informations");
                }
                DisplayStudents(dtpLessons.Value, IdLesson, IdClass);
            }
        }

        /// <summary>
        /// Actualise la liste des cours et l'affichage des élèves
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbxClasses_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnFillWithSelectedMode.Enabled = false;
            IdClass = ((KeyValuePair<int, string>)cbxClasses.SelectedItem).Key;
            int lessons = UpdateLessonsComboBox((int)dtpLessons.Value.DayOfWeek);
            IdLesson = 0;
            if (lessons > 0)
            {
                IdLesson = ((KeyValuePair<int, string>)cbxLessons.SelectedItem).Key;
            }
            DisplayStudents(dtpLessons.Value, IdLesson, IdClass);
            btnSavePresences.Enabled = false;

        }

        private void cbxLessons_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxLessons.SelectedItem != null)
            {
                if (cbxLessons.SelectedItem.ToString() != "Aucun cours")
                {
                    if (cbxLessons.Items.Count > 0)
                    {
                        IdClass = ((KeyValuePair<int, string>)cbxClasses.SelectedItem).Key;
                        IdLesson = ((KeyValuePair<int, string>)cbxLessons.SelectedItem).Key;
                        DisplayStudents(dtpLessons.Value, IdLesson, IdClass);
                    }
                }
                else
                {
                    dgvPresences.Rows.Clear();
                }
            }
            btnSavePresences.Enabled = false;

        }

        /// <summary>
        /// Met à jour la liste des cours en fonction du jour de la semaine et de la classe. Retourne le nombre de cours récupérés
        /// </summary>
        /// <param name="dayOfTheWeek"></param>
        /// <param name="idClass"></param>
        /// <returns></returns>
        private int UpdateLessonsComboBox(int dayOfTheWeek)
        {
            Dictionary<int, string> lessons = MC.GetLessonsByTeacher(dayOfTheWeek, IdClass);
            if (lessons.Count > 0)
            {
                cbxLessons.DataSource = new BindingSource(lessons, null);
                cbxLessons.DisplayMember = "Value";
                cbxLessons.ValueMember = "Key";
                return lessons.Count;
            }
            else
            {
                cbxLessons.DataSource = null;
                cbxLessons.Items.Clear();
                cbxLessons.Items.Add("Aucun cours");
                cbxLessons.SelectedIndex = 0;
                return -1;
            }
        }

        private void lblLogin_Click(object sender, EventArgs e)
        {
            FrmEditPassword frmEditPwd = new FrmEditPassword(MC);
            frmEditPwd.ShowDialog();
        }

        private void lblLogin_MouseEnter(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void lblLogin_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        /// <summary>
        /// Affiche une popup avec les informations détaillées de l'élève
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvPresences_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            //On vérifie que la cellule cliquée est sur la bonne colonne pour ne pas éxecuter le code partout
            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewTextBoxColumn && e.RowIndex >= 0 && e.ColumnIndex == 1)
            {
                int idPeople = (int)senderGrid.Rows[e.RowIndex].Cells[COL_IDPEOPLE].Value;
                People p = MC.GetPeopleById(idPeople);
                string infos = String.Format("Prénom : {0}\r\nNom : {1}\r\nDate de naissance : {2:dd.MM.yyy}\r\nEmail : {3}\r\nNuméro de téléphone : {4}", p.firstName, p.name, p.birthDate, p.email, p.phone);
                MessageBox.Show(infos);
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            FrmHelp frmHelp = new FrmHelp();
            frmHelp.ShowDialog();
        }
    }
}
