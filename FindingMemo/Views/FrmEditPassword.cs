﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using FindingMemo.Controllers;
using System;
using System.Windows.Forms;

namespace FindingMemo.Views {
    public partial class FrmEditPassword : Form
    {
        private Tools _t;
        private MemoController _mC;

        public Tools T { get => _t; set => _t = value; }
        public MemoController MC { get => _mC; set => _mC = value; }

        public FrmEditPassword(MemoController m)
        {
            InitializeComponent();
            T = new Tools(this);
            MC = m;
        }

        public void TextBox_TextChanged(object sender, EventArgs e)
        {
            btnSave.Enabled = tbxOldPassword.Text != string.Empty && tbxNewPassword.Text == tbxConfirmNewPassword.Text && (tbxNewPassword.Text != string.Empty && tbxConfirmNewPassword.Text != string.Empty);
        }

        private void TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            T.TitleBar_MouseDown(sender, e);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string oldPassword = tbxOldPassword.Text;
            string newPassword = tbxNewPassword.Text;
            if (!MC.EditPassword(oldPassword, newPassword))
            {
                MessageBox.Show("Des informations saisies sont incorrectes. Veuillez les vérifier");
            }
            else
            {
                MessageBox.Show("Mot de passe modifié avec succès");
                Close();
            }
        }
    }
}
