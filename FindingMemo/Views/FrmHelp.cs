﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using System;
using System.Windows.Forms;

namespace FindingMemo.Views {
    public partial class FrmHelp : Form
    {
        private Tools _t;
        public Tools T { get => _t; set => _t = value; }
        public FrmHelp()
        {
            InitializeComponent();
            T = new Tools(this);
        }

        private void TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            T.TitleBar_MouseDown(sender, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
