﻿namespace FindingMemo.Views {
    partial class FrmLessonsAssignement {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.CbxLessons = new System.Windows.Forms.ComboBox();
            this.CbxTeachers = new System.Windows.Forms.ComboBox();
            this.CbxDaysOfTheWeek = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnAssign = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.CbxClasses = new System.Windows.Forms.ComboBox();
            this.DgvAssignements = new System.Windows.Forms.DataGridView();
            this.ClmnIdLesson = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClmnIdClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClmnIdTeacher = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClmnLesson = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClmnDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClmnClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ClmnTeacher = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnEdit = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.CbxAssignMode = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAssignements)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Montserrat SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(9, 12);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(235, 15);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "FindingMemo - Assignement des cours";
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(576, 5);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(31, 28);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "X";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.MediumAquamarine;
            this.pnlTitleBar.Controls.Add(this.lblTitle);
            this.pnlTitleBar.Controls.Add(this.btnExit);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(610, 38);
            this.pnlTitleBar.TabIndex = 2;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // CbxLessons
            // 
            this.CbxLessons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxLessons.FormattingEnabled = true;
            this.CbxLessons.Location = new System.Drawing.Point(12, 67);
            this.CbxLessons.Name = "CbxLessons";
            this.CbxLessons.Size = new System.Drawing.Size(121, 21);
            this.CbxLessons.TabIndex = 3;
            // 
            // CbxTeachers
            // 
            this.CbxTeachers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxTeachers.FormattingEnabled = true;
            this.CbxTeachers.Location = new System.Drawing.Point(474, 67);
            this.CbxTeachers.Name = "CbxTeachers";
            this.CbxTeachers.Size = new System.Drawing.Size(121, 21);
            this.CbxTeachers.TabIndex = 4;
            // 
            // CbxDaysOfTheWeek
            // 
            this.CbxDaysOfTheWeek.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxDaysOfTheWeek.FormattingEnabled = true;
            this.CbxDaysOfTheWeek.Location = new System.Drawing.Point(166, 67);
            this.CbxDaysOfTheWeek.Name = "CbxDaysOfTheWeek";
            this.CbxDaysOfTheWeek.Size = new System.Drawing.Size(121, 21);
            this.CbxDaysOfTheWeek.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Sélectionner un cours";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(478, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Assigner un enseignant\r\n";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(163, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Choisissez la date du cours";
            // 
            // BtnAssign
            // 
            this.BtnAssign.Location = new System.Drawing.Point(509, 5);
            this.BtnAssign.Name = "BtnAssign";
            this.BtnAssign.Size = new System.Drawing.Size(75, 23);
            this.BtnAssign.TabIndex = 9;
            this.BtnAssign.Text = "Assigner";
            this.BtnAssign.UseVisualStyleBackColor = true;
            this.BtnAssign.Click += new System.EventHandler(this.BtnAssign_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(317, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Sélectionner la classe";
            // 
            // CbxClasses
            // 
            this.CbxClasses.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxClasses.FormattingEnabled = true;
            this.CbxClasses.Location = new System.Drawing.Point(320, 67);
            this.CbxClasses.Name = "CbxClasses";
            this.CbxClasses.Size = new System.Drawing.Size(121, 21);
            this.CbxClasses.TabIndex = 10;
            // 
            // DgvAssignements
            // 
            this.DgvAssignements.AllowUserToAddRows = false;
            this.DgvAssignements.AllowUserToDeleteRows = false;
            this.DgvAssignements.AllowUserToResizeColumns = false;
            this.DgvAssignements.AllowUserToResizeRows = false;
            this.DgvAssignements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgvAssignements.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ClmnIdLesson,
            this.ClmnIdClass,
            this.ClmnIdTeacher,
            this.ClmnLesson,
            this.ClmnDay,
            this.ClmnClass,
            this.ClmnTeacher});
            this.DgvAssignements.Location = new System.Drawing.Point(12, 94);
            this.DgvAssignements.Name = "DgvAssignements";
            this.DgvAssignements.ReadOnly = true;
            this.DgvAssignements.RowHeadersVisible = false;
            this.DgvAssignements.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DgvAssignements.Size = new System.Drawing.Size(584, 309);
            this.DgvAssignements.TabIndex = 12;
            this.DgvAssignements.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvAssignements_CellClick);
            // 
            // ClmnIdLesson
            // 
            this.ClmnIdLesson.Frozen = true;
            this.ClmnIdLesson.HeaderText = "idLesson";
            this.ClmnIdLesson.Name = "ClmnIdLesson";
            this.ClmnIdLesson.ReadOnly = true;
            this.ClmnIdLesson.Visible = false;
            // 
            // ClmnIdClass
            // 
            this.ClmnIdClass.Frozen = true;
            this.ClmnIdClass.HeaderText = "idClass";
            this.ClmnIdClass.Name = "ClmnIdClass";
            this.ClmnIdClass.ReadOnly = true;
            this.ClmnIdClass.Visible = false;
            // 
            // ClmnIdTeacher
            // 
            this.ClmnIdTeacher.Frozen = true;
            this.ClmnIdTeacher.HeaderText = "idTeacher";
            this.ClmnIdTeacher.Name = "ClmnIdTeacher";
            this.ClmnIdTeacher.ReadOnly = true;
            this.ClmnIdTeacher.Visible = false;
            // 
            // ClmnLesson
            // 
            this.ClmnLesson.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ClmnLesson.HeaderText = "Cours";
            this.ClmnLesson.Name = "ClmnLesson";
            this.ClmnLesson.ReadOnly = true;
            // 
            // ClmnDay
            // 
            this.ClmnDay.HeaderText = "Jour de la semaine";
            this.ClmnDay.Name = "ClmnDay";
            this.ClmnDay.ReadOnly = true;
            // 
            // ClmnClass
            // 
            this.ClmnClass.HeaderText = "Classe";
            this.ClmnClass.Name = "ClmnClass";
            this.ClmnClass.ReadOnly = true;
            // 
            // ClmnTeacher
            // 
            this.ClmnTeacher.HeaderText = "Enseignant";
            this.ClmnTeacher.Name = "ClmnTeacher";
            this.ClmnTeacher.ReadOnly = true;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Enabled = false;
            this.BtnEdit.Location = new System.Drawing.Point(146, 5);
            this.BtnEdit.Name = "BtnEdit";
            this.BtnEdit.Size = new System.Drawing.Size(129, 23);
            this.BtnEdit.TabIndex = 13;
            this.BtnEdit.Text = "Modifier l\'assignement";
            this.BtnEdit.UseVisualStyleBackColor = true;
            this.BtnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Enabled = false;
            this.BtnDelete.Location = new System.Drawing.Point(0, 5);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(129, 23);
            this.BtnDelete.TabIndex = 14;
            this.BtnDelete.Text = "Supprimer l\'assignement";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // CbxAssignMode
            // 
            this.CbxAssignMode.AutoSize = true;
            this.CbxAssignMode.Checked = true;
            this.CbxAssignMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbxAssignMode.Enabled = false;
            this.CbxAssignMode.Location = new System.Drawing.Point(387, 9);
            this.CbxAssignMode.Name = "CbxAssignMode";
            this.CbxAssignMode.Size = new System.Drawing.Size(116, 17);
            this.CbxAssignMode.TabIndex = 15;
            this.CbxAssignMode.Text = "Mode Assignement";
            this.CbxAssignMode.UseVisualStyleBackColor = true;
            this.CbxAssignMode.CheckedChanged += new System.EventHandler(this.CbxAssignMode_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnDelete);
            this.panel1.Controls.Add(this.CbxAssignMode);
            this.panel1.Controls.Add(this.BtnEdit);
            this.panel1.Controls.Add(this.BtnAssign);
            this.panel1.Location = new System.Drawing.Point(12, 409);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(584, 31);
            this.panel1.TabIndex = 16;
            // 
            // FrmLessonsAssignement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(610, 444);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.DgvAssignements);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CbxClasses);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CbxDaysOfTheWeek);
            this.Controls.Add(this.CbxTeachers);
            this.Controls.Add(this.CbxLessons);
            this.Controls.Add(this.pnlTitleBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmLessonsAssignement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmLessonsAssignement";
            this.Load += new System.EventHandler(this.FrmLessonsAssignement_Load);
            this.pnlTitleBar.ResumeLayout(false);
            this.pnlTitleBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvAssignements)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Panel pnlTitleBar;
        private System.Windows.Forms.ComboBox CbxLessons;
        private System.Windows.Forms.ComboBox CbxTeachers;
        private System.Windows.Forms.ComboBox CbxDaysOfTheWeek;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtnAssign;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CbxClasses;
        private System.Windows.Forms.DataGridView DgvAssignements;
        private System.Windows.Forms.Button BtnEdit;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClmnIdLesson;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClmnIdClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClmnIdTeacher;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClmnLesson;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClmnDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClmnClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClmnTeacher;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.CheckBox CbxAssignMode;
        private System.Windows.Forms.Panel panel1;
    }
}