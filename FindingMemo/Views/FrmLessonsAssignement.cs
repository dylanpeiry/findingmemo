﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using FindingMemo.Controllers;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FindingMemo.Views
{
    public partial class FrmLessonsAssignement : Form
    {

        private int _idClassSelected;
        private int _idLessonSelected;
        private int _dayOfLessonSelected;

        private Tools _t;

        private FrmPeopleManagement _peopleManagement;

        private Tools T { get => _t; set => _t = value; }

        public FrmPeopleManagement PeopleManagement { get => _peopleManagement; set => _peopleManagement = value; }

        public MemoController MC { get => PeopleManagement.MC; }
        private int IdClassSelected { get => _idClassSelected; set => _idClassSelected = value; }
        private int IdLessonSelected { get => _idLessonSelected; set => _idLessonSelected = value; }
        private int DayOfLessonSelected { get => _dayOfLessonSelected; set => _dayOfLessonSelected = value; }

        private enum DaysOfTheWeek
        {
            Lundi = 1,
            Mardi = 2,
            Mercredi = 3,
            Jeudi = 4,
            Vendredi = 5,
            Samedi = 6,
            Dimanche = 7
        }

        public FrmLessonsAssignement(FrmPeopleManagement frmPeopleManagement)
        {
            InitializeComponent();
            T = new Tools(this);
            PeopleManagement = frmPeopleManagement;
        }

        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            T.TitleBar_MouseDown(sender, e);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmLessonsAssignement_Load(object sender, EventArgs e)
        {
            // Fill the combobox
            List<Lessons> listLessons = MC.GetAllLessons();
            List<People> listTeachers = MC.GetAllTeachers();
            List<Classes> listClasses = MC.GetAllClasses();

            CbxLessons.DataSource = listLessons;
            CbxLessons.DisplayMember = "labelLesson";
            CbxLessons.ValueMember = "idLesson";

            CbxTeachers.DataSource = listTeachers;
            CbxTeachers.DisplayMember = "name";
            CbxTeachers.ValueMember = "idPeople";

            CbxClasses.DataSource = listClasses;
            CbxClasses.DisplayMember = "labelClass";
            CbxClasses.ValueMember = "idClass";

            CbxDaysOfTheWeek.DataSource = Enum.GetValues(typeof(DaysOfTheWeek));

            // Fill the datagridview
            FillDataGridViewAssignements();
        }

        private void FillDataGridViewAssignements()
        {
            DgvAssignements.Rows.Clear();

            List<Plannings> allPlannings = MC.GetAllPlannings();
            foreach (Plannings planning in allPlannings)
            {
                // Get the data
                int idLesson = planning.idLesson;
                int idClass = planning.idClass;
                int idTeacher = (int)planning.idTeacher;
                string lessonLabel = MC.GetLessonsById(planning.idLesson).labelLesson;
                DaysOfTheWeek dayOfTheWeek = (DaysOfTheWeek)planning.dayOfLesson;
                string day = dayOfTheWeek.ToString();
                string classes = MC.GetClassesById(planning.idClass).labelClass;
                string teacherName = MC.GetPeopleById((int)planning.idTeacher).name;

                // Create the new row
                AddNewRowToDataGridView(idLesson, idClass, idTeacher, lessonLabel, day, classes, teacherName);
            }
        }

        private void AddNewRowToDataGridView(int idLesson, int idClass, int idTeacher, string lessonLabel, string day, string classes, string teacherName)
        {
            // Create the new row
            int index = DgvAssignements.Rows.Add();
            DgvAssignements.Rows[index].Cells["ClmnIdLesson"].Value = idLesson;
            DgvAssignements.Rows[index].Cells["ClmnIdClass"].Value = idClass;
            DgvAssignements.Rows[index].Cells["ClmnIdTeacher"].Value = idTeacher;
            DgvAssignements.Rows[index].Cells["ClmnLesson"].Value = lessonLabel;
            DgvAssignements.Rows[index].Cells["ClmnDay"].Value = day;
            DgvAssignements.Rows[index].Cells["ClmnClass"].Value = classes;
            DgvAssignements.Rows[index].Cells["ClmnTeacher"].Value = teacherName;
        }

        private void BtnAssign_Click(object sender, EventArgs e)
        {
            // Get the data from the combobox
            int idLesson = (int)CbxLessons.SelectedValue;
            int idTeacher = (int)CbxTeachers.SelectedValue;
            int idClass = (int)CbxClasses.SelectedValue;
            int dayOfTheWeek = (int)CbxDaysOfTheWeek.SelectedValue;

            if (!MC.AssignTeacherAndClassesToLesson(idLesson, idClass, idTeacher, dayOfTheWeek))
            {
                MessageBox.Show("Ce cours est déjà assigné à cette classe pour le même jour de la semaine", "Echec de l'assignement", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                FillDataGridViewAssignements();
            }

        }

        private void DgvAssignements_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // Get the id's of the assignements
            int indexSelectedRow = DgvAssignements.SelectedCells[0].RowIndex;

            int indexIdClass = ClmnIdClass.Index;
            IdClassSelected = (int)DgvAssignements[indexIdClass, indexSelectedRow].Value;

            int indexIdLesson = ClmnIdLesson.Index;
            IdLessonSelected = (int)DgvAssignements[indexIdLesson, indexSelectedRow].Value;

            int indexIdTeacher = ClmnIdTeacher.Index;
            int idTeacherSelected = (int)DgvAssignements[indexIdTeacher, indexSelectedRow].Value;

            CbxClasses.SelectedValue = IdClassSelected;
            CbxLessons.SelectedValue = IdLessonSelected;
            CbxTeachers.SelectedValue = idTeacherSelected;

            // The combobox of the days don't have value member, so we have to use select the corresponding day by string match
            int indexIdDayOfLesson = ClmnDay.Index;
            string dayOfLessonSelected = DgvAssignements[indexIdDayOfLesson, indexSelectedRow].Value.ToString();
            CbxDaysOfTheWeek.SelectedIndex = CbxDaysOfTheWeek.FindStringExact(dayOfLessonSelected);

            // To save the day selected for later, we need to parse the enum by a string and return the int value corresponding
            DaysOfTheWeek day;
            DayOfLessonSelected = (Enum.TryParse(dayOfLessonSelected, out day)) ? (int)day : -1;

            // Disable the assign mode
            CbxAssignMode.Checked = false;
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            // Get the data from the combobox
            int newIdLesson = (int)CbxLessons.SelectedValue;
            int newIdTeacher = (int)CbxTeachers.SelectedValue;
            int newIdClass = (int)CbxClasses.SelectedValue;
            int newDayOfTheWeek = (int)CbxDaysOfTheWeek.SelectedValue;

            if (MC.EditAssignement(IdLessonSelected, DayOfLessonSelected, IdClassSelected, newDayOfTheWeek, newIdClass, newIdTeacher))
            {
                MessageBox.Show("L'édition a correctement fonctionné", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                // After a edit we pass to the assign mode
                CbxAssignMode.Checked = true;
                FillDataGridViewAssignements();
            }
            else
            {
                MessageBox.Show("Une erreur est survenue lors de l'édition", "Info", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            string messageWarning = "Vous vous apprêtez à supprimer cet assignement, cette action est irréversible ! \nÊtes-vous toujours sûr de vouloir le faire ?";
            string title = "Attention !";
            DialogResult dr = MessageBox.Show(messageWarning, title, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (dr == DialogResult.Yes)
            {
                if (MC.DeleteAssignement(IdLessonSelected, IdClassSelected, DayOfLessonSelected))
                {
                    MessageBox.Show("La suppression a correctement fonctionné.");
                }
                else
                {
                    MessageBox.Show("Une erreure est survenue lors de la suppression.");
                }
                // After a delete we pass to the assign mode
                CbxAssignMode.Checked = true;
                FillDataGridViewAssignements();
            }
        }

        private void CbxAssignMode_CheckedChanged(object sender, EventArgs e)
        {
            if (CbxAssignMode.Checked)
            {
                // Mode assign on, so we enable the assign button
                BtnAssign.Enabled = true;
                // Next we disable the edition & suppression button and checkbox
                BtnEdit.Enabled = false;
                BtnDelete.Enabled = false;
                CbxAssignMode.Enabled = false;
            }
            else
            {
                // Mode add off, so we disable the add button
                BtnAssign.Enabled = false;
                // Next we enable the edition & suppresion button and checkbox
                BtnEdit.Enabled = true;
                BtnDelete.Enabled = true;
                CbxAssignMode.Enabled = true;
            }
        }
    }
}
