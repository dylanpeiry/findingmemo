﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using FindingMemo.Controllers;
using System;
using System.Windows.Forms;

namespace FindingMemo.Views {
    public partial class FrmLessonsManagement : Form {
        const int MAX_LABEL_LESSON_LENGTH = 32;

        private int __idLessonSelected;

        private string _lessonLabel;

        private string _descriptionLabel;

        private Tools _t;

        private FrmPeopleManagement _peopleManagement;

        private Tools T { get => _t; set => _t = value; }

        public FrmPeopleManagement PeopleManagement { get => _peopleManagement; set => _peopleManagement = value; }

        private MemoController MC { get => PeopleManagement.MC; }
        public int IdLessonSelected { get => __idLessonSelected; set => __idLessonSelected = value; }
        public string LessonLabel { get => _lessonLabel; set => _lessonLabel = value; }
        public string DescriptionLabel { get => _descriptionLabel; set => _descriptionLabel = value; }
        

        public FrmLessonsManagement(FrmPeopleManagement frmPeopleManagement) {
            InitializeComponent();
            T = new Tools(this);
            PeopleManagement = frmPeopleManagement;
        }

        private void FrmEditLessons_Load(object sender, EventArgs e) {
            this.lessonsTableAdapter.Fill(this.findingMemoDBDataSet.Lessons);
        }

        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e) {
            T.TitleBar_MouseDown(sender, e);
        }

        private void btnExit_Click(object sender, EventArgs e) {
            Close();
        }

        private void BtnAdd_Click(object sender, EventArgs e) {
            if (VerifyFields()) {
                MC.AddLesson(LessonLabel, DescriptionLabel);
                this.lessonsTableAdapter.Fill(this.findingMemoDBDataSet.Lessons);
                TbxLabel.Text = string.Empty;
                TbxDescription.Text = string.Empty;
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e) {
            if (VerifyFields()) {
                MC.EditLesson(IdLessonSelected, LessonLabel, DescriptionLabel);
                this.lessonsTableAdapter.Fill(this.findingMemoDBDataSet.Lessons);
                CbxAddMode.Checked = true;
            }

        }

        private bool VerifyFields() {
            LessonLabel = TbxLabel.Text.Trim();
            DescriptionLabel = TbxDescription.Text.Trim();

            if (LessonLabel.Length > 0 && LessonLabel.Length < MAX_LABEL_LESSON_LENGTH && DescriptionLabel.Length > 0) {
                return true;
            } else {
                return false;
            }
        }

        private void CbxMode_CheckedChanged(object sender, EventArgs e) {
            if (CbxAddMode.Checked) {
                // Mode add on, so we enable the add button and clear the fields
                BtnAdd.Enabled = true;
                TbxLabel.Text = string.Empty;
                TbxDescription.Text = string.Empty;
                // Next we disable the edition & suppression button and checkbox
                BtnEdit.Enabled = false;
                BtnDelete.Enabled = false;
                CbxAddMode.Enabled = false;
            } else {
                // Mode add off, so we disable the add button
                // (we don't clear the fields because they will be filled automatically)
                BtnAdd.Enabled = false;
                // Next we enable the edition & suppresion button and checkbox
                BtnEdit.Enabled = true;
                BtnDelete.Enabled = true;
                CbxAddMode.Enabled = true;
            }
        }

        private void DgvLessons_CellClick(object sender, DataGridViewCellEventArgs e) {
            // Get the id of the selected lesson
            int indexSelectedRow = DgvLessons.SelectedCells[0].RowIndex;
            int indexIdLesson = ClmnIdLesson.Index;
            IdLessonSelected = (int)DgvLessons[indexIdLesson, indexSelectedRow].Value;

            Lessons l = MC.GetLessonsById(IdLessonSelected);
            TbxLabel.Text = l.labelLesson;
            TbxDescription.Text = l.description;
            CbxAddMode.Checked = false;
        }

        private void BtnDelete_Click(object sender, EventArgs e) {
            string messageWarning = "Vous vous apprêtez à supprimer ce cours, cette action est irréversible et entrainera la suppression de toutes les données liés au cours. \nÊtes-vous toujours sûr de vouloir le faire ?";
            string title = "Attention !";
            DialogResult dr = MessageBox.Show(messageWarning, title, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (dr == DialogResult.Yes) {
                if (MC.DeleteLesson(IdLessonSelected)) {
                    MessageBox.Show("La suppression a correctement fonctionné.");
                    this.lessonsTableAdapter.Fill(this.findingMemoDBDataSet.Lessons);
                } else {
                    MessageBox.Show("Une erreure est survenue lors de la suppression.");
                }
                // After a delete we pass to the add mode
                CbxAddMode.Checked = true;
            }
        }
    }
}
