﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using FindingMemo.Controllers;
using FindingMemo.Views;
using System;
using System.Windows.Forms;

namespace FindingMemo {
    public partial class FrmLogin : Form
    {
        private MemoController _mc;
        private Tools _t;
        public MemoController MC { get => _mc; set => _mc = value; }
        public Tools T { get => _t; set => _t = value; }

        public FrmLogin()
        {
            InitializeComponent();
            MC = new MemoController();
            T = new Tools(this);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            btnLogin.Enabled = (tbxLogin.Text != String.Empty && tbxPassword.Text != String.Empty);
        }

        private void TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            T.TitleBar_MouseDown(sender, e);
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string login = tbxLogin.Text;
            string password = tbxPassword.Text;

            if (MC.VerifyLogin(login, password))
            {
                if (!MC.IsConnectedUserAdmin())
                {
                    FrmDashboard dashboard = new FrmDashboard(this);
                    dashboard.ShowDialog();
                }
                else
                {
                    FrmPeopleManagement pManagement = new FrmPeopleManagement(this);
                    pManagement.ShowDialog();
                }
                tbxLogin.Text = string.Empty;
                tbxPassword.Text = string.Empty;
            }
            else
            {
                MessageBox.Show("Nom d'utilisateur ou mot de passe incorrect.");
            }
        }
    }
}
