﻿namespace FindingMemo.Views {
    partial class FrmPeopleManagement {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.findingMemoDBDataSet = new FindingMemo.FindingMemoDBDataSet();
            this.peopleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.peopleTableAdapter = new FindingMemo.FindingMemoDBDataSetTableAdapters.PeopleTableAdapter();
            this.tableAdapterManager = new FindingMemo.FindingMemoDBDataSetTableAdapters.TableAdapterManager();
            this.peopleDataGridView = new System.Windows.Forms.DataGridView();
            this.ClmnIdPeople = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnAddPeople = new System.Windows.Forms.Button();
            this.BtnEditPeople = new System.Windows.Forms.Button();
            this.BtnLessons = new System.Windows.Forms.Button();
            this.btnClasses = new System.Windows.Forms.Button();
            this.pnlOtherManagement = new System.Windows.Forms.Panel();
            this.BtnAssignements = new System.Windows.Forms.Button();
            this.pnlStudentsManagement = new System.Windows.Forms.Panel();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.findingMemoDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peopleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peopleDataGridView)).BeginInit();
            this.pnlOtherManagement.SuspendLayout();
            this.pnlStudentsManagement.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.Salmon;
            this.pnlTitleBar.Controls.Add(this.btnExit);
            this.pnlTitleBar.Controls.Add(this.lblTitle);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(633, 38);
            this.pnlTitleBar.TabIndex = 1;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(595, 3);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(36, 32);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "X";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Montserrat SemiBold", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(10, 13);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(297, 15);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "FindingMemo - Gestion des enseignants et élèves";
            this.lblTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // findingMemoDBDataSet
            // 
            this.findingMemoDBDataSet.DataSetName = "FindingMemoDBDataSet";
            this.findingMemoDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // peopleBindingSource
            // 
            this.peopleBindingSource.DataMember = "People";
            this.peopleBindingSource.DataSource = this.findingMemoDBDataSet;
            // 
            // peopleTableAdapter
            // 
            this.peopleTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ClassesTableAdapter = null;
            this.tableAdapterManager.LessonsTableAdapter = null;
            this.tableAdapterManager.PeopleTableAdapter = this.peopleTableAdapter;
            this.tableAdapterManager.PlanningsTableAdapter = null;
            this.tableAdapterManager.PresencesTableAdapter = null;
            this.tableAdapterManager.StatusTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = FindingMemo.FindingMemoDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // peopleDataGridView
            // 
            this.peopleDataGridView.AllowUserToAddRows = false;
            this.peopleDataGridView.AllowUserToDeleteRows = false;
            this.peopleDataGridView.AllowUserToResizeColumns = false;
            this.peopleDataGridView.AllowUserToResizeRows = false;
            this.peopleDataGridView.AutoGenerateColumns = false;
            this.peopleDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.peopleDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ClmnIdPeople,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11});
            this.peopleDataGridView.DataSource = this.peopleBindingSource;
            this.peopleDataGridView.Location = new System.Drawing.Point(15, 92);
            this.peopleDataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.peopleDataGridView.Name = "peopleDataGridView";
            this.peopleDataGridView.ReadOnly = true;
            this.peopleDataGridView.RowHeadersVisible = false;
            this.peopleDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.peopleDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.peopleDataGridView.Size = new System.Drawing.Size(603, 350);
            this.peopleDataGridView.TabIndex = 2;
            // 
            // ClmnIdPeople
            // 
            this.ClmnIdPeople.DataPropertyName = "idPeople";
            this.ClmnIdPeople.HeaderText = "idPeople";
            this.ClmnIdPeople.Name = "ClmnIdPeople";
            this.ClmnIdPeople.ReadOnly = true;
            this.ClmnIdPeople.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nom";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "firstName";
            this.dataGridViewTextBoxColumn3.HeaderText = "Prénom";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "birthDate";
            this.dataGridViewTextBoxColumn4.HeaderText = "Date de naissance";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "email";
            this.dataGridViewTextBoxColumn5.HeaderText = "Email";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "phone";
            this.dataGridViewTextBoxColumn6.HeaderText = "Téléphone";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "isTeacher";
            this.dataGridViewTextBoxColumn7.FalseValue = "0";
            this.dataGridViewTextBoxColumn7.HeaderText = "Est enseignant";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewTextBoxColumn7.TrueValue = "1";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "login";
            this.dataGridViewTextBoxColumn8.HeaderText = "Identifiant";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "password";
            this.dataGridViewTextBoxColumn9.HeaderText = "Mot de passe";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "idClass";
            this.dataGridViewTextBoxColumn10.HeaderText = "idClass";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "isAdmin";
            this.dataGridViewTextBoxColumn11.HeaderText = "isAdmin";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // BtnAddPeople
            // 
            this.BtnAddPeople.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAddPeople.Location = new System.Drawing.Point(0, 2);
            this.BtnAddPeople.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnAddPeople.Name = "BtnAddPeople";
            this.BtnAddPeople.Size = new System.Drawing.Size(226, 28);
            this.BtnAddPeople.TabIndex = 4;
            this.BtnAddPeople.Text = "Ajouter un élève ou un enseignant";
            this.BtnAddPeople.UseVisualStyleBackColor = true;
            this.BtnAddPeople.Click += new System.EventHandler(this.BtnAddPeople_Click);
            // 
            // BtnEditPeople
            // 
            this.BtnEditPeople.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEditPeople.Location = new System.Drawing.Point(378, 2);
            this.BtnEditPeople.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnEditPeople.Name = "BtnEditPeople";
            this.BtnEditPeople.Size = new System.Drawing.Size(226, 28);
            this.BtnEditPeople.TabIndex = 5;
            this.BtnEditPeople.Text = "Modifier la personne sélectionnée";
            this.BtnEditPeople.UseVisualStyleBackColor = true;
            this.BtnEditPeople.Click += new System.EventHandler(this.BtnEditPeople_Click);
            // 
            // BtnLessons
            // 
            this.BtnLessons.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLessons.Location = new System.Drawing.Point(441, 5);
            this.BtnLessons.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnLessons.Name = "BtnLessons";
            this.BtnLessons.Size = new System.Drawing.Size(163, 28);
            this.BtnLessons.TabIndex = 6;
            this.BtnLessons.Text = "Gestion des cours";
            this.BtnLessons.UseVisualStyleBackColor = true;
            this.BtnLessons.Click += new System.EventHandler(this.BtnLessons_Click);
            // 
            // btnClasses
            // 
            this.btnClasses.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClasses.Location = new System.Drawing.Point(-1, 5);
            this.btnClasses.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnClasses.Name = "btnClasses";
            this.btnClasses.Size = new System.Drawing.Size(163, 28);
            this.btnClasses.TabIndex = 3;
            this.btnClasses.Text = "Gestion des classes";
            this.btnClasses.UseVisualStyleBackColor = true;
            this.btnClasses.Click += new System.EventHandler(this.btnClasses_Click);
            // 
            // pnlOtherManagement
            // 
            this.pnlOtherManagement.Controls.Add(this.BtnAssignements);
            this.pnlOtherManagement.Controls.Add(this.BtnLessons);
            this.pnlOtherManagement.Controls.Add(this.btnClasses);
            this.pnlOtherManagement.Location = new System.Drawing.Point(15, 52);
            this.pnlOtherManagement.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlOtherManagement.Name = "pnlOtherManagement";
            this.pnlOtherManagement.Size = new System.Drawing.Size(603, 35);
            this.pnlOtherManagement.TabIndex = 7;
            // 
            // BtnAssignements
            // 
            this.BtnAssignements.Font = new System.Drawing.Font("Montserrat", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAssignements.Location = new System.Drawing.Point(215, 5);
            this.BtnAssignements.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnAssignements.Name = "BtnAssignements";
            this.BtnAssignements.Size = new System.Drawing.Size(175, 28);
            this.BtnAssignements.TabIndex = 7;
            this.BtnAssignements.Text = "Gestion des assignations";
            this.BtnAssignements.UseVisualStyleBackColor = true;
            this.BtnAssignements.Click += new System.EventHandler(this.BtnAssignements_Click);
            // 
            // pnlStudentsManagement
            // 
            this.pnlStudentsManagement.Controls.Add(this.BtnAddPeople);
            this.pnlStudentsManagement.Controls.Add(this.BtnEditPeople);
            this.pnlStudentsManagement.Location = new System.Drawing.Point(14, 451);
            this.pnlStudentsManagement.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlStudentsManagement.Name = "pnlStudentsManagement";
            this.pnlStudentsManagement.Size = new System.Drawing.Size(604, 32);
            this.pnlStudentsManagement.TabIndex = 8;
            // 
            // FrmPeopleManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 495);
            this.Controls.Add(this.pnlStudentsManagement);
            this.Controls.Add(this.pnlOtherManagement);
            this.Controls.Add(this.peopleDataGridView);
            this.Controls.Add(this.pnlTitleBar);
            this.Font = new System.Drawing.Font("Montserrat", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmPeopleManagement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmPeopleManagement";
            this.Load += new System.EventHandler(this.FrmPeopleManagement_Load);
            this.pnlTitleBar.ResumeLayout(false);
            this.pnlTitleBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.findingMemoDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peopleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peopleDataGridView)).EndInit();
            this.pnlOtherManagement.ResumeLayout(false);
            this.pnlStudentsManagement.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTitleBar;
        private System.Windows.Forms.Label lblTitle;
        private FindingMemoDBDataSet findingMemoDBDataSet;
        private System.Windows.Forms.BindingSource peopleBindingSource;
        private FindingMemoDBDataSetTableAdapters.PeopleTableAdapter peopleTableAdapter;
        private FindingMemoDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView peopleDataGridView;
        private System.Windows.Forms.Button btnClasses;
        private System.Windows.Forms.Button BtnAddPeople;
        private System.Windows.Forms.Button BtnEditPeople;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClmnIdPeople;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.Button BtnLessons;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnLessons;
        private System.Windows.Forms.Panel pnlOtherManagement;
        private System.Windows.Forms.Panel pnlStudentsManagement;
        private System.Windows.Forms.Button BtnAssignements;
    }
}