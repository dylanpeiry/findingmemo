﻿/* Project : FindingMemo
 * Description : Créer une application qui permet la gestion des présences dans une salle de cours.
 * Author : PEIRY Dylan & GENGA Dario
 * Version : 1
 * Date : 22.11.2018
 */
using FindingMemo.Controllers;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FindingMemo.Views {
    public partial class FrmPeopleManagement : Form {
        private Tools _tools;
        private FrmLogin _login;
        public Tools Tools { get => _tools; set => _tools = value; }

        public FrmLogin Login { get => _login; set => _login = value; }

        public MemoController MC { get => Login.MC; }

        public FrmPeopleManagement(FrmLogin frmLogin) {
            InitializeComponent();
            Tools = new Tools(this);
            Login = frmLogin;
            Login.MC = new MemoController();
            Login.Visible = false;
        }


        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e) {
            Tools.TitleBar_MouseDown(sender, e);
        }

        private void btnExit_Click(object sender, EventArgs e) {
            DialogResult dr = MessageBox.Show("Voulez vous vraiment quitter l'application ? Vous allez être déconnectez.", "Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes) {
                Close();
                Login.Visible = true;
            }
        }

        private void FrmPeopleManagement_Load(object sender, EventArgs e) {
            ReloadPeople();
        }

        private void BtnAddPeople_Click(object sender, EventArgs e) {
            FrmAddPeople frmAddPeople = new FrmAddPeople(this);
            frmAddPeople.ShowDialog();
        }

        public void ReloadPeople() {
            List<People> people = MC.GetAllPeopleExceptAdmin();
            peopleDataGridView.DataSource = people;
        }

        private void BtnEditPeople_Click(object sender, EventArgs e) {
            // Get the id of the selected people
            int indexSelectedRow = peopleDataGridView.SelectedCells[0].RowIndex;
            int indexIdPeople = ClmnIdPeople.Index;
            int idPeopleSelected = (int)peopleDataGridView[indexIdPeople, indexSelectedRow].Value;

            FrmAddPeople frmAddPeople;
            People p = MC.GetPeopleById(idPeopleSelected);

            if (p.isTeacher == 1) {
                // Edit a teacher
                frmAddPeople = new FrmAddPeople(this, idPeopleSelected, p.name, p.firstName, p.email, p.birthDate, p.phone, p.login, p.password);
            } else {
                // Edit a student
                frmAddPeople = new FrmAddPeople(this, idPeopleSelected, p.name, p.firstName, p.email, p.birthDate, p.phone, p.idClass);
            }
            frmAddPeople.ShowDialog();
        }


        private void BtnLessons_Click(object sender, EventArgs e) {
            FrmLessonsManagement frmEditLessons = new FrmLessonsManagement(this);
            frmEditLessons.ShowDialog();
        }
        
        private void btnClasses_Click(object sender, EventArgs e)
        {
            FrmClassesManagement frm = new FrmClassesManagement(this);
            frm.ShowDialog();

        }

        private void BtnAssignements_Click(object sender, EventArgs e) {
            FrmLessonsAssignement frmLessonsAssignement = new FrmLessonsAssignement(this);
            frmLessonsAssignement.ShowDialog();
        }
    }
}
